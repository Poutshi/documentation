 * Download the theme :

git clone [https://github.com/vjeantet/hugo-theme-docdock.git](https://github.com/vjeantet/hugo-theme-docdock.git)

 * Organize the folder to meet go hugo structure
 * It is possible to rename the folder using the Hugo CLI  
