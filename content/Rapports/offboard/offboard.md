# FCU : Flight Control Unit

The FCU is the part of the system commanding the uav.

Using a serial connection we communicate with the Pixhawk in OFFBOARD mode.

## OFFBOARD

### Prerequisites

  - ROS installed
  - QGroundControl installed
  - Download [MAVROS example](https://dev.px4.io/en/ros/mavros_offboard.html) or use our code

The offboard mode is used to send mavlink command via the companion computer.

<div class="info">
Passing to offboard mode requires to **send positions
messages** and to **continue sending** those messages to keep the
connection alive.
</div>

![connection graph](fcu/connection_graph.png){width=75%}

### Launching Offboard Mode

In order to be able to control the offboard mode with a switch on the
radio controller it needs to be configured in
QGroundControl Two setups are possible :

  - Use offboard switch to override current mode.
  - Use offboard in the list of modes
  - Use software to ask for offboard mode 

### Offboard Switch

The offboard switch is a function that when toggled the offboard mode overrides any running mode. Don't forget to set the RC\_OFFB\_TH, this parameter allows to set when a switch detects the high position (from 0 to 1, if the switch has 3 positions put above 0.5 to use only the last position). When the switch is flipped again the fcu goes back to the mode given by the flight mode channel (or base mode if a flight mode channel was not set).

### Offboard Flight Mode

Offboard can be configured like any flight mode (for example : altitude, stabilized, ...) but in order to not be rejected it needs the sending of position via MAVLink. If the position are not sent the previous flight mode stays on.

### Our Setup

In order to be safe we use both of the solutions proposed above and map
them onto the channel 6. The **RC\_OFFB\_TH** was put to 0.7.

![radio setup](fcu/radio_setup.png){width=75%}

| Channel 6  | Flight Mode |
| ---------- | ----------- |
| Position 1 | Altitude    |
| Position 2 | Stabilized  |
| Position 3 | Offboard    |

This corresponds to the following setup on the radio command :

![radio switches](fcu/radio.png){width=75%}

### Software Offboard Trigger

Using ROS or c code the offboard mode can be triggered overriding any currently used flight mode. This methods is using the [MAVLink] **COMMAND\_LONG**

<div class="info"> 
This is considered dangerous and therefore is not advised
</div>

##### Lost connection

If the offboard connection a failsafe is launched to be programmed in the QGroundControl parameters. (Two failsafe are available one if the RC
fails too **COM\_OBL\_ACT** and one if the RC is still accessible **COM\_OBL\_RC\_ACT**

### Offboard Commands

Once the offboard connection is established we can start sending
commands, in our case we publish command which can take the form of
either a x,y,z position or vx,vy,vz velocity.

<div class="info">
Don't forget to do **ros.spinOnce()** to refresh the **publish** of pose/velocity and the **subscribed** topic to have update of the current position of the drone
</div>
