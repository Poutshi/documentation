# QGroundControl 

## QGroundControl Install

Install QGroundControl for Ubuntu Linux 14.04 LTS or later. You can
either install the AppImage or the compressed archive.

### AppImage

1.  Download QGroundControl (Donwload
    [link](https://docs.qgroundcontrol.com/en/getting_started/download_and_install.html)
2.  Give the permission and launch

    sudo chmod +x ./QGroundControl.AppImage
    QGroundControl.AppImage

### Compressed archive

1.  Download [QGroundControl.tar.bz2](https://s3-us-west-2.amazonaws.com/qgroundcontrol/latest/QGroundControl.tar.bz2).
2.  Extract the archive using the terminal command:

```bash  
tar jxf QGroundControl.tar.bz2
cd qgroundcontrol
./qgroundcontrol-start.sh
```

## SITL QGC connection

 - Connect QGControl to the simulated PX4
 - Disable auto-connect

![autoconnect](../../static/media/simulation/disable_autoconnect.png){width=500px}

  - Create a new udp connection

![udp](../../static/media/simulation/creat_udp.png){width=500px}
 
 - Connect to UDP

![connect](../../static/media/simulation/qgcconnect.png){width=500px}


* * * * * * * * * *