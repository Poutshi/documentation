<!-- <meta http-equiv="refresh" content="3"/> -->

# RotorS simulator 
<!-- {#rotors} -->

## Simuted parts

  - Body
  - Motors
  - Aerodynamic effects
  - Sensors : IMU, odometry, visual inertial sensor, user developed sensors. 
  - Simple controller : Geometric Controller (cf [@lee2010geometric.)
  - State estimation : position, orientation, linear and angular velocity.
      - Ideal estimator given by a Gazebo plugin
      - Multi Sensor Fusion (MSF) framework [@lynen2013robust] [lynen](https://www.research-collection.ethz.ch/bitstream/handle/20.500.11850/79512/eth-7648-01.pdf?sequence=1)

![building block of the RotorS simulator](../../static/media/simulation/buildingblock.png)

## Controller

![Controller architecture](../../static/media/simulation/controller.png)

## State estimation

IMU : attitude measurement. For pose estimation use : vision, laser, maps, SLAM, Vicon, marker based localisation.
 * For closed loop visual navigation see [@achtelik2014advanced]
 * For attitude estimation using Indirect Kalman Filter see [@trawny2005indirect]
 * For control using vision see [@weiss2012vision]
  <!-- - *Stephan Weiss. Vision based navgation for micro helicpters. PhD
    thesis, ETH Zurich, 2012.*
  - *Markus W. Achtelik. Advance Closed Loop Viual Navigation for
    MicroAerial Vehicles. PhD thesis, ETH Zurich, 2014.*
  - *N. Trawny and S. I. Roumeliotis. Indirect Kalman filter for 3D
    attitude estimation. Technical Report 2005-002, University of
    Minnesota, Dept. of Computer Science and Engineering, 2005.* -->

## Simulator overview

![Simulation overview](../../static/media/simulation/sim_overview.png)

| Sensor   | Description                                                                                                | Xacro-name  Gazebo plugin(s)           |
| -------- | ---------------------------------------------------------------------------------------------------------- | -------------------------------------- |
| Camera   | A configurable standard ROS camera                                                                         | camera_macro                           |
|          |                                                                                                            | libgazebo_ros_camera.so                |
| IMU      | This is an IMU implementation, with zero mean white Gaussian noise                                         | imu\_plugin\_macro                     |
|          | and a random walk on all measurements                                                                      | librotors\_gazebo\_imu\_plugin.so      |
| Odometry | Simulate position, pose, and odometry sensors.                                                             | librotors\_gazebo\_odometry\_plugin.so |
|          | A binary image can be attached to specify where the sensor works in the world,                             | odometry\_plugin\_macro                |
|          | in order to simulate sensor outages. White pixel: sensor works, black: sensor does not work.               |                                        |
|          | It mimics any generic on- or off-board tracking system such as Global Positioning System (GPS), Vicon, etc |                                        |


## Using the simulator

 - Install the rotors_simulator package
 - Install the dependencies :
    - Mavlink
    - octomap (optional)
    - joy(optional)

```bash
    $  sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu `lsb_release 
    
    $  sudo apt-get install ros-kinetic-joy ros-kinetic-octomap-ros ros-kinetic-mavlink python-wstool python-catkin-tools protobuf-compiler libgoogle-glog-dev ros-kinetic-control-toolbox
    
    $  sudo rosdep init
    $  rosdep update
    $  source /opt/ros/kinetic/setup.bash
    
    $  mkdir -p ~/catkin_ws/src
    $  cd ~/catkin_ws/src
    $  catkin_init_workspace  # initialize your catkin workspace
    $  wstool init
    
    $  wget https://raw.githubusercontent.com/ethz-asl/rotors_simulator/master/rotors_hil.rosinstall
    $  wstool merge rotors_hil.rosinstall
    $  wstool update

    $  cd ~/catkin_ws/
    $  catkin build
```

### Procedure

1.  Select your model
 * Pick one of the models provided by the RostorS, or
 * Build your own model
2.  Attach sensors to the MAV 
 * Use one of the Xacro for the sensors shipped with RotorS, or
 * Create your own sensor (see [gazebosim](http://gazebosim.org/tutorials?cat=sensors) for references) and attach them directly, or create a Xacro, analogously to the Xacro in components_snippets.xacro
3.  Add your controller to your MAV
 * Start one of the controllers shipped with RotorS, or
 * Write your own controller, and launch it.
4.  Use a state estimator
 * Do not use a state estimator, and use the output of the ideal odometry sensor directly as an input to your controller, or
 * Use MSF (developped by ETHZ ASL)
 * Use you own state estimator.

#### Hovering example

    roslaunch rotors_gazebo mav_hovering_example.launch

* * * * * * * * * *