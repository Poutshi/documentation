# Simulators

Simulation is the easiest way to test control architectures and
algorithms without the stress of crashing a machine. Moreover it allows
to collect data fastly and repeatedly. By using a simulation costs are
reduced, and robots can be programmed off-line which eliminates any
down-time for an assembly line.

Sense and avoid is a problem falling in the *Sensor-based* control
family. *Sensor-based* robot actions are much more difficult to simulate
and/or to program off-line, since the robot motion depends on the
instantaneous sensor readings in the real world.

## Known Simulators

There is no such thing as the best simulator. A non exhaustive list of
robotics simulation tools may contain :

##### Robotics

  -  Actin
  -  ARS
  -  Gazebo
  -  MORSE
  -  OpenHRP
  -  RoboDK
  -  SimSpark
  -  V-Rep
  -  Webots
  -  4DV-Sim
  -  OpenRAVE

##### Helicopter

  -  Host and RiseHost and Rise

##### Multicopter

  -  JMAVSim, a uav simulator created by the ETH-Zurich. It supports all
    kind of multicopter and has a strong dependency to MAVLink oriented
    UAV.JMAVSim, a uav simulator created by the ETH-Zurich. It supports
    all kind of multicopter and has a strong dependency to MAVLink
    oriented UAV.

##### General purpose

  -  Matlab Simulink, is more a framework for control theory including
    some nice simulation tools.Matlab Simulink, is more a framework for
    control theory including some nice simulation tools.

## Choosen simulators

The main features requested in the simulator are :

  -  The applications can be transfered onto physical robot without
    modification.The applications can be transfered onto physical robot
    without modification.
  -  The simulator should provide a 3D visual rendering.The simulator
    should provide a 3D visual rendering.
  -  Ideally it should use a physics engine for more realistic motion
    generation.Ideally it should use a physics engine for more realistic
    motion generation.
  -  Fast prototyping using a creation tool or can import from common
    CAO softwares (Catia, Solidworks…)Fast prototyping using a creation
    tool or can import from common CAO softwares (Catia, Solidworks…)
  -  Easy to interface with robotic tools and framework.Easy to
    interface with robotic tools and framework.

## Gazebo informations

|                           |                                                                      |
| ------------------------- | -------------------------------------------------------------------- |
| Developper                | *[Open Source Robotics Foundation](http://osrfoundation.org) (OSRF)* |
| Development status        | *Active*                                                             |
| License                   | *Apache 2.0*                                                         |
| 3D rendering engine       | *OGRE*                                                               |
| Physics engine            | *Open Dynamics Engine|ODE /Bullet/Simbody/DART (software)*           |
| 3D modeller               | *Internal*                                                           |
| Platforms supported       | *Linux*                                                              |
| Main programming language |                                                                      |


* * * * * * * * * *