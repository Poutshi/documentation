# Development environment on Linux 

 * You can build for all PX4 targets (NuttX based hardware, Qualcomm Snapdragon Flight hardware, linux-base hardware, Simulation, ROS)
 * Standard distro is Ubuntu 16.04 


## Disable modmanager and add the user to the group "dialou"
1. sudo apt-get remove modemmanager
2. On the command prompt enter:
   sudo usermod -a -G dialout $USER
3. Logout and login again (the change is only made after a new login).


## Manual Installation

### Common dependencies

```bash
sudo apt-get update -y
sudo apt-get install git zip qtcreator cmake \
    build-essential genromfs ninja-build exiftool vim-common -y
# Required python packages
sudo apt-get install python-argparse \
    python-empy python-toml python-numpy python-yaml \
    python-dev python-pip -y
sudo -H pip install --upgrade pip 
sudo -H pip install pandas jinja2 pyserial cerberus
# optional python tools
sudo -H pip install pyulog
```
### Ninja Build System 
Ninja is a faster build system than Make and the PX4 CMake generators support it. 
```bash
sudo apt-get install ninja-build -y
```

### FastRTPS installation
```bash
wget http://www.eprosima.com/index.php/component/ars/repository/eprosima-fast-rtps/eprosima-fast-rtps-1-5-0/eprosima_fastrtps-1-5-0-linux-tar-gz -O eprosima_fastrtps-1-5-0-linux.tar.gz
tar -xzf eprosima_fastrtps-1-5-0-linux.tar.gz eProsima_FastRTPS-1.5.0-Linux/
tar -xzf eprosima_fastrtps-1-5-0-linux.tar.gz requiredcomponents
tar -xzf requiredcomponents/eProsima_FastCDR-1.0.7-Linux.tar.gz
(cd eProsima_FastCDR-1.0.7-Linux && ./configure --libdir=/usr/lib && make -j2 && sudo make install)
(cd eProsima_FastRTPS-1.5.0-Linux && ./configure --libdir=/usr/lib && make -j2 && sudo make install)
rm -rf requiredcomponents eprosima_fastrtps-1-5-0-linux.tar.gz
```

### NuttX-based Hardware

```bash
sudo apt-get install python-serial openocd \
    flex bison libncurses5-dev autoconf texinfo \
    libftdi-dev libtool zlib1g-dev -y
sudo apt-get remove gcc-arm-none-eabi gdb-arm-none-eabi binutils-arm-none-eabi gcc-arm-embedded
sudo add-apt-repository --remove ppa:team-gcc-arm-embedded/ppa
pushd .
cd ~
wget https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-rm/7-2017q4/gcc-arm-none-eabi-7-2017-q4-major-linux.tar.bz2
tar -jxf gcc-arm-none-eabi-7-2017-q4-major-linux.tar.bz2
exportline="export PATH=$HOME/gcc-arm-none-eabi-7-2017-q4-major/bin:\$PATH"
if grep -Fxq "$exportline" ~/.profile; then echo nothing to do ; else echo $exportline >> ~/.profile; fi
popd
```

# Building the PX4 Software 

 * First install the developer toolchain.

## Download PX4 source code 

 * Fork the repo 
 * Clone it in your directory 

## Building for NuttX boards

 * For Pixhawk 2 run make for FMUv3

### Building 
``` bash
cd Firmware 
make px4_fmu-v4_default
```

 * For versions anterior to 1.9.0 use   
``` bash
cd Firmware 
make nuttx_px4fmu-v3_default
```

### Uploading
 * Connect the board to the host in usb. The make finds the board and uploads the firmware automatically

```bash
make px4_fmu-v3_default upload
```
* For versions anterior to 1.9.0 use   
``` bash
cd Firmware 
make nuttx_px4fmu-v3_default upload
```