# UAVs in simulation

## Introduction

To simulate uav we nedd : 

 * Model of the uav kinematics
 * Model of the uav dynamics
 * A model of the flight controler unit (fcu)
 * A way to teleoperate the uav
 * (optional) a ground control station (gcs)

## UAV model : 
**_Iris or F550_**

Kinematics and dynamics are provided by the RotorS simulators [@furrer2016rotors]. It is developed by the Autonomous System Lab at ETH Zurich (ETHZ-ASL). It provides some multirotor models such as the AscTec Hummingbird, the AscTec Pelican, or the AscTec Firefly, but the simulator is not limited for the use with these multicopters. There are simulated sensors coming with the simulator such as an IMU, a generic odometry sensor, and the VI-Sensor, which can be mounted on the multirotor. This packages also contains some example controllers, basic worlds, a joystick interface, and example launch files. Below we provide the instructions necessary for getting started. See RotorS' wiki for more instructions and examples [rotors_simulator](https://github.com/ethz-asl/rotors_simulator/wiki). 

See section [RotorS Simulator]


## FCU simulation
**_PX4 sitl_**

see chapter [SITL Simulation]

## Teleoperation
**_Futaba's Interlink Controller_**

For teleoperation we use a joystick or a gamepad connected to the gcs. 

List of tested joysticks :

 * ![ps4 controller with wire [buy](https://www.ldlc.com/fiche/PB00215310.html) ](simulation/ps4.png "this is a picture of a ps4 controller" ){ width=200px }

 * ![Xbox controller for PC [buy](https://www.ldlc.com/fiche/PB00241250.html) ](simulation/xbox.png){ width=200px }

 * ![Logitech joystick for PC [buy](https://www.ldlc.com/fiche/PB00147749.html)](simulation/logitech_joy.png){width=200px}

 * ![Interlink Elite Controller by Futaba [buy](https://drlracingsimulator.zendesk.com/hc/en-us/articles/115001169552-Interlink-Elite-RealFlight)](simulation/realflight6.png){width=200px}



The best choice, if available, is the Futaba's Interlink Elite Controller. Using this controller we can seamlessly migrate the code to the real uav. Moreover doing so we can test all the configurations in simulation and avoid surprises when testing in the real world. 


## Ground Control Station
**_QGroundControl_**

We choose the ground control software : [QGroundControl](http://qgroundcontrol.com/). QGroundControl (qgc)  provides full flight control and mission planning for any MAVLink enabled uav. QGC is also the ground station we use in the real flight setup. Using the same ground control software in simulation reduces the differences between the simulation setup and real flight setup which reduces the risks.

See section [QGroundControl] for more informations

## Communication protocol
**_MAVLink_**

Communication in the simulation and in the real world is made through MAVLink. 

See section [MAVLink] for more info. 

* * * * * * * * * *