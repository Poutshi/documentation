# SITL Simulation

PX4 provides a software in the loop (SITL) simulation. The PX4 firmware is compiled for x86 architectures it reads sensors data for a simulated world and sends motor control to the simulated uav. The sitl depends on the simulator Gazebo and the RotorS plugins (see [RotorS simulator])

## prerequisites

  - ROS (kinetic) 
  - Gazebo7 

### Installs:

  - Common dependencies libraries and tools as defined in `ubuntu_sim_common_deps.sh`
  - ROS Kinetic (including Gazebo7)
  - MAVROS
  - Common dependencies and tools for all targets (including: Ninja build system, Qt Creator, pyulog)
  - PX4/Firmware source (to ~/src/Firmware/)

    sudo usermod -a -G dialout $USER
    source ubuntu_sim_common_deps.sh
    sudo apt-get remove modemmanager



#### One liner

On a clean system you can use the file

[ubuntu_sim.sh](https://raw.githubusercontent.com/PX4/Devguide/master/build_scripts/ubuntu_sim.sh): This is a bash script for setting up a ROS/Gazebo development
environment for PX4 on Ubuntu LTS (16.04). It installs the common dependencies for all targets (including Qt Creator) and the ROS Kinetic/Gazebo 7 (the default).

#### Install Dependencies

[ubuntu_sim_common_deps.sh](https://raw.githubusercontent.com/PX4/Devguide/master/build_scripts/ubuntu_sim_common_deps.sh): This file is a Bash script for setting up a PX4 development environment on Ubuntu LTS (16.04). It can be used for installing simulators (only) or for installing the preconditions for Snapdragon Flight or Raspberry Pi.

* Other dependencies


    sudo apt-get install libprotobuf-dev libprotoc-dev protobuf-compiler libeigen3-dev \
    gazebo7 libgazebo7-dev libxml2-utils python-rospkg python-jinja2


### Interaction with RotorS Simulator

![sim_sitl.png](simulation/sim_sitl.png)

## SITL Gazebo

RotorS simulator models are now integrated to PX4 simulation. Models and plugins are stored in the folder `Tools/sitl_gazebo` 

This is a flight simulator for multirotors, VTOL and fixed wing. It uses
the motor model and other pieces from the RotorS simulator, but in
contrast to RotorS has no dependency on ROS.

If you want to use the sitl_gazebo folder in another location than the
default one (inside the Firmware folder) compile it using :

    cd sitl_gazebo
    git submodule update --init --recursive
    mkdir -p build 
    cd build 
    cmake ..
    make -j2

<div class="warning">
  <!-- <img src="styles/icons/warning.png" alt="warning" style="float:left" > -->
  <p>  Use the `-j` option with caution. It can lead to an excessive memory usage crashing the computer</p>
</div>
<!-- \<WRAP alert\> \</WRAP\> -->

### Environment variables

    GAZEBO_MODEL_PATH=/usr/share/gazebo-7/models:
    GAZEBO_RESOURCE_PATH=/usr/share/gazebo-7:/usr/share/gazebo_models:
    GAZEBO_MASTER_URI=http://localhost:11345
    GAZEBO_PLUGIN_PATH=/usr/lib/x86_64-linux-gnu/gazebo-7/plugins:
    GAZEBO_MODEL_DATABASE_URI=http://gazebosim.org/models


## PX4 folder organization

  - **build** : Contains build results
  - **mavlink** : an include directory of mavlink headers.
  - **msg** : protobuf message definition
  - **src** : PX4 flight stack sources.
  - **Tools** : 
      - shell, python scripts
      - jMAVSim, Matlab, Gazebo simulation

The simulated PX4 firware is located  :

`~/src/Firmware/build/posix_sitl_default/px4`


## Launch the sitl 

`make posix_sitl_default gazebo`

The simulation contained in the firmware repository [firmware](https://github.com/PX4/Firmware) depends on the sitl_gazebo submodule [sitl_gazebo](https://github.com/PX4/sitl_gazebo)

We can separate the sitl launch from the simulation files. This way we have a better understanding of what's happening when we launch the global simulation with the following command :

`make posix_sitl_default gazebo`

Moreover, having two separate directories make it easier to interface with another flight controller than the PX4.

### Step by step launch 

#### Usage

``` 
./px4 [-d] [data_directory] startup_config [-h]
   -d            - Optional flag to run the app in daemon mode and does not listen for user input.
                   This is needed if px4 is intended to be run as a upstart job on linux
<data_directory> - directory where ROMFS and posix-configs are located (if not given, CWD is used)
<startup_config> - config file for starting/stopping px4 modules
   -h            - help/usage information

```

#### Launch demo

    cd ~/src/Firmware/posix-configs
    ./../build/posix_sitl_default/px4 SITL/init/ekf2/iris

`SITL/init/ekf2/iris` : is a config file specific to an Iris quadcopter using ekf2 as a pose estimator.


The build system makes it very easy to build and start PX4 on SITL, launch a simulator, and connect them. For example, you can launch a SITL version of PX4 that uses the EKF2 estimator and simulate a plane in Gazebo with just the following command (provided all the build and gazebo dependencies are present !):

    make posix_sitl_ekf2 gazebo_plane

<div class="info">
It is also possible to separately build and start SITL and the various simulators, but this is nowhere near as "turnkey".
</div>

<!-- \<WRAP info \> \</WRAP\> -->

The syntax to call `make` with a particular configuration and
initialisation file is:

    make [CONFIGURATION_TARGET] [VIEWER_MODEL_DEBUGGER]

where:

  - CONFIGURATION_TARGET : has the format    `[OS][_PLATFORM][_FEATURE]`
      - OS: posix, nuttx, qurt
      - PLATFORM: SITL (or in principle any platform supported among the
        different OS: bebop, eagle, excelsior, etc.)
      - FEATURE: A particular high level feature - for example which
        estimator to use (ekf2, lpe) or to run tests or simulate using a
        replay.

<div class="info"> 
You can get a list of all available configuration targets using the command:

`make list_config_targets` 
</div>

  - VIEWER_MODEL_DEBUGGER: has the format
    `[SIMULATOR]_[MODEL][_DEBUGGER]`
      - SIMULATOR: This is the simulator ("viewer") to launch and connect: gazebo, jmavsim
      - MODEL: The vehicle model to use (e.g. iris, rover, tailsitter, etc). This corresponds to a specific initialisation file that will be used to configure PX4. This might define the start up for a particular vehicle, or allow simulation of multiple vehicles (we explain how to find available init files in the next section).
      - DEBUGGER: Debugger to (optionally) use: none, ide, gdb, lldb, ddd, valgrind, callgrind.

<div class="info">
You can get a list of all available VIEWER_MODEL_DEBUGGER options using the command: 

`make posix list_vmd_make_targets` 
</div>

Notes:

  - Most of the values in the `CONFIGURATION_TARGET` and `VIEWER_MODEL_DEBUGGER` have defaults, and are hence optional. For example, `gazebo` is equivalent to `gazebo_iris` or `gazebo_iris_none`.
  - You can use three underscores if you want to specify a default value between two other settings. For example, `gazebo___gdb` is equivalent to `gazebo_iris_gdb`.
  - You can use a none value for `VIEWER_MODEL_DEBUGGER` to start PX4 and wait for a simulator. For example start PX4 using `make posix_sitl_default none` and jMAVSim using `./Tools/jmavsim_run.sh`.

#### Running the default simulation

We suppose that the Pixhawk firmware is cloned in
`$HOME/$USER/src/Firmware`

``` 
cd ~/src/Firmware
make posix_sitl_default gazebo

```

This will call the simulation launch file located in `Tools.sitl_run.sh`. This file usage is as :

    sitl_run.sh rc_script rcS_dir debugger program model src_path build_path

The default simulation has the following arguments :

  - **sitl_bin** or **rc_script** :
    `~/Firmware/build/posix_sitl_default/px4`
  - **rcS_dir** : `posix-configs/SITL/init/ekf2`
  - **debugger** : none
  - **program** : `gazebo`
  - **model** : none
  - **src_path**: `~/Firmware`
  - **build_path**: `~/Firmware/build/posix_sitl_default`

A gazebo window should appear containing a quadcopter and a rough
terrain.

![gzclient](simulation/default_gzclient_camera_1_-2018-05-28t12_03_11.317274.jpg){width=750px}


<!-- next px4_ros.md px4_sim.md -->

***********************************