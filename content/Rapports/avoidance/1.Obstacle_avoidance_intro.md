# Obstacle avoidance for UAV

## Introduction 

In order to fly safely UAVs should be able to detect their environment and avoid harmfull obstacles.

Obstacle avoidance is a key behavior for autonomous vehicles . It is the ability of a system to :

-   Detect obstacles.
-   Track obstacles.
-   recognize obstacles.
-   Generate safe trajectory

In the following document I will discuss 3 of those points. Obstacle recognition is a desirable behaviour but it is not mandatory. I will present at the end of the document some techniques that can be helpful to study obstacle recognition.

## Development logic

~~~graphviz
digraph{
    label="Obstacle avoidance development logic"

    start[shape="box", style=rounded];
    k[shape="box", style=rounded];
    /*if[shape="diamond", style=""]*/
    node[shape="box", style=""]
        a;d;e;f;g;h;i;j;k;
    /*input[shape="box"]*/

    start -> a 
    a -> d
    d -> e
    e -> f[label="OK"]
    f -> g[label="OK"]
    g -> h[label="OK"]
    h -> i[label="OK"]
    i -> j[label="OK"]
    j -> k[label="OK"]

    f -> e[label="NOK"]
    g -> e[label="NOK"]
    h -> e[label="NOK"]
    i -> e[label="NOK"]
    j -> e[label="NOK"]


    a[label="High level specification, \ncontext definition, \nBibliography"]
    d[label="Select best \"N\" methods and algorithms"]
    e[label="Pick method N+1"]
    f[label="Desktop test : \nSimulation with Gazebo using a 2D mobile robot"]
    g[label="Lab test : \nEmbed on a mobile robot"]
    h[label="Migrate implementation to a 3D environment"]
    i[label="Desktop test : \nSimulation with Gazebo using a UAV model"]
    j[label="Embed on real UAV"]
    k[label="Pass to the test and integration team"]

}
~~~

<!-- ## Document division

 * 2D obstacle avoidance
   * Material
   * Algo
 * 3D obstacle avoidance
   * Material 
   * Algo
 * Simulation
   * 3D simlulation
   * 2D simulation -->










***************************************************










