# Zed Cam ROS

## Installation 

### Dependencies

```bash
sudo apt install libpcl1 
sudo apt install ros-kinetic-pcl-ros
```

### Clone and build from sources

```bash
git clone https://github.com/stereolabs/zed-ros-wrapper.git
```

### Launch files

```mermaid
graph LR
zed.launch --> zed_camera.launch
zed_camera.launch --> zed_wrapper_node
zed_camera.launch --> zed_state_publisher
```
```mermaid
graph LR
display.launch --> zed.launch
display.launch --> rviz
```

### RTBMAP tutorial

```mermaid
graph LR
zed_rtabmap --> zed_camera.launch
zed_rtabmap --> rtabmap.launch
```























