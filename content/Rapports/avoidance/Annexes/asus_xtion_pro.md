# ASUS XTION PRO 

## Specifications 

| Image size        | VGA (640x480) 30 fps QVGA (320 x 240) 60fps          |
| ----------------- | ---------------------------------------------------- |
| FOV               | 58° H, 45° V, 70° D (Horizontal, Vertical, Diagonal) |
| Distance of use   | Between 0.8m and 3.5m                                |
| Power consumption | Below 2.5W                                           |
| Interface         | USB 2.0                                              |
| Environment       | Indoor                                               |
| Dimension,        | 18 x 3.5 x 5                                         |

## Installation

### Openni2 installation (ubuntu 16.04)

#### USB check

1. Check if the camera is detected 

   ``` bash
   lsusb
   Bus 002 Device 049: ID 1d27:0600 ASUS 
   ```

2. Check if the device is correctly mounted. Unplug the replug the device **ON AN USB 2.0 port** and check the dmesg

   ```bas
   dmesg 
   [ 9180.483844] usb 1-1: new high-speed USB device number 5 using xhci_hcd
   [ 9180.635484] usb 1-1: New USB device found, idVendor=1d27, idProduct=0600
   [ 9180.635491] usb 1-1: New USB device strings: Mfr=2, Product=1, SerialNumber=0
   [ 9180.635495] usb 1-1: Product: PrimeSense Device
   [ 9180.635499] usb 1-1: Manufacturer: PrimeSense
   [ 9180.636183] usb 1-1: Not enough bandwidth for new device state.
   [ 9180.636241] usb 1-1: can't set config #1, error -28
   ```

   This output means that you should desactivate **xHCI** and **EHCI Hand-off** in your BIOS setup. A good outpout should look like the following : 

   ```bash
   [10370.239395] usb 2-1.5: new high-speed USB device number 53 using ehci-pci
   [10370.349005] usb 2-1.5: New USB device found, idVendor=04e8, idProduct=6860
   [10370.349014] usb 2-1.5: New USB device strings: Mfr=2, Product=3, SerialNumber=4
   [10370.349019] usb 2-1.5: Product: SAMSUNG_Android
   [10370.349024] usb 2-1.5: Manufacturer: SAMSUNG
   [10370.349030] usb 2-1.5: SerialNumber: ce0718279becb50d04
   [10791.904893] usb 2-1.1.4: USB disconnect, device number 49
   [10848.959551] usb 2-1.1.4: new high-speed USB device number 54 using ehci-pci
   [10849.075072] usb 2-1.1.4: New USB device found, idVendor=1d27, idProduct=0600
   [10849.075078] usb 2-1.1.4: New USB device strings: Mfr=2, Product=1, SerialNumber=0
   [10849.075081] usb 2-1.1.4: Product: PrimeSense Device
   [10849.075084] usb 2-1.1.4: Manufacturer: PrimeSense
   [10874.335810] usb 2-1.5: USB disconnect, device number 53
   [10950.589353] usb 2-1.5: new high-speed USB device number 55 using ehci-pci
   [10950.700029] usb 2-1.5: New USB device found, idVendor=04e8, idProduct=6860
   [10950.700035] usb 2-1.5: New USB device strings: Mfr=2, Product=3, SerialNumber=4
   [10950.700039] usb 2-1.5: Product: SAMSUNG_Android
   [10950.700043] usb 2-1.5: Manufacturer: SAMSUNG
   [10950.700046] usb 2-1.5: SerialNumber: ce0718279becb50d04
   ```



#### Install dependencies

```bash
sudo apt-get install -y g++ git python libusb-1.0-0-dev libudev-dev freeglut3-dev doxygen graphviz openjdk-8-jdk
```

#### Install for 64 bit pc

```bash
git clone https://github.com/occipital/OpenNI2.git
cd OpenNI2
PLATFORM=x64 make
```

### Install for Arm target

For Odroid or Jetson TX2

```bash
cd OpenNI2
find . -iname platform.arm
gedit ./ThirdParty/PSCommon/BuildSystem/Platform.Arm
```

1. Replace the line : 

```makefile
CFLAGS += -march=armv7-a -mtune=cortex-a8 -mfpu=neon -mfloat-abi=hard
```

With the line

```make
CFLAGS += -march=armv7-a -mtune=cortex-a15 -mfpu=neon-vfpv4 -mfloat-abi=hard
```

2. Add support for pthread library

```bash
gedit ThirdParty/PSCommon/BuildSystem/CommonCppMakefile
```

Replace paragraph 

```makefile
ifneq "$(EXE_NAME)" ""
        OUTPUT_NAME = $(EXE_NAME)
        # We want the executables to look for the .so's locally first:
        LDFLAGS += -Wl,-rpath ./
        OUTPUT_COMMAND = $(CXX) -o $(OUTPUT_FILE) $(OBJ_FILES) $(LDFLAGS)
endif
```

By the line : 

```makefile
ifneq "$(EXE_NAME)" ""
        OUTPUT_NAME = $(EXE_NAME)
        # We want the executables to look for the .so's locally first:
        LDFLAGS += -Wl,-rpath ./
        ifneq (“$(OSTYPE)”,”Darwin”)
    		LDFLAGS += -lpthread
		endif
        OUTPUT_COMMAND = $(CXX) -o $(OUTPUT_FILE) $(OBJ_FILES) $(LDFLAGS)
endif
```

3. Compile

   ```bash
   PLATFORM=Arm make
   ```

#### Run the default test

```bash
cd OpenNI2/Bin/Arm-Release
./NiViewer
```

## Using with ROS 

#### Install the packages

```bash
sudo apt install ros-kinetic-openni2-launch ros-kinetic-openni2-camera
```

#### Launch the demo

```bash
roslaunch openni2_launch openni2.launch
```

#### NODES 

```bash
/camera/camera_nodelet_manager
/camera/depth_metric
/camera/depth_metric_rect
/camera/depth_points
/camera/depth_rectify_depth
/camera/depth_registered_sw_metric_rect
/camera/driver
/camera/points_xyzrgb_sw_registered
/camera/register_depth_rgb
/camera/rgb_rectify_color
/camera_base_link
/camera_base_link1
/camera_base_link2
/camera_base_link3
```

#### TOPICS 

```bash
/camera/depth/camera_info
/camera/depth/image
/camera/depth/image/compressed
/camera/depth/image/compressed/parameter_descriptions
/camera/depth/image/compressed/parameter_updates
/camera/depth/image/compressedDepth
/camera/depth/image/compressedDepth/parameter_descriptions
/camera/depth/image/compressedDepth/parameter_updates
/camera/depth/image/theora
/camera/depth/image/theora/parameter_descriptions
/camera/depth/image/theora/parameter_updates
/camera/depth/image_raw
/camera/depth/image_raw/compressed
/camera/depth/image_raw/compressed/parameter_descriptions
/camera/depth/image_raw/compressed/parameter_updates
/camera/depth/image_raw/compressedDepth
/camera/depth/image_raw/compressedDepth/parameter_descriptions
/camera/depth/image_raw/compressedDepth/parameter_updates
/camera/depth/image_raw/theora
/camera/depth/image_raw/theora/parameter_descriptions
/camera/depth/image_raw/theora/parameter_updates
/camera/depth/image_rect
/camera/depth/image_rect/compressed
/camera/depth/image_rect/compressed/parameter_descriptions
/camera/depth/image_rect/compressed/parameter_updates
/camera/depth/image_rect/compressedDepth
/camera/depth/image_rect/compressedDepth/parameter_descriptions
/camera/depth/image_rect/compressedDepth/parameter_updates
/camera/depth/image_rect/theora
/camera/depth/image_rect/theora/parameter_descriptions
/camera/depth/image_rect/theora/parameter_updates
/camera/depth/image_rect_raw
/camera/depth/image_rect_raw/compressed
/camera/depth/image_rect_raw/compressed/parameter_descriptions
/camera/depth/image_rect_raw/compressed/parameter_updates
/camera/depth/image_rect_raw/compressedDepth
/camera/depth/image_rect_raw/compressedDepth/parameter_descriptions
/camera/depth/image_rect_raw/compressedDepth/parameter_updates
/camera/depth/image_rect_raw/theora
/camera/depth/image_rect_raw/theora/parameter_descriptions
/camera/depth/image_rect_raw/theora/parameter_updates
/camera/depth/points
/camera/depth_rectify_depth/parameter_descriptions
/camera/depth_rectify_depth/parameter_updates
/camera/depth_registered/camera_info
/camera/depth_registered/image_raw
/camera/depth_registered/image_raw/compressed
/camera/depth_registered/image_raw/compressed/parameter_descriptions
/camera/depth_registered/image_raw/compressed/parameter_updates
/camera/depth_registered/image_raw/compressedDepth
/camera/depth_registered/image_raw/compressedDepth/parameter_descriptions
/camera/depth_registered/image_raw/compressedDepth/parameter_updates
/camera/depth_registered/image_raw/theora
/camera/depth_registered/image_raw/theora/parameter_descriptions
/camera/depth_registered/image_raw/theora/parameter_updates
/camera/depth_registered/points
/camera/depth_registered/sw_registered/camera_info
/camera/depth_registered/sw_registered/image_rect
/camera/depth_registered/sw_registered/image_rect/compressed
/camera/depth_registered/sw_registered/image_rect/compressed/parameter_descriptions
/camera/depth_registered/sw_registered/image_rect/compressed/parameter_updates
/camera/depth_registered/sw_registered/image_rect/compressedDepth
/camera/depth_registered/sw_registered/image_rect/compressedDepth/parameter_descriptions
/camera/depth_registered/sw_registered/image_rect/compressedDepth/parameter_updates
/camera/depth_registered/sw_registered/image_rect/theora
/camera/depth_registered/sw_registered/image_rect/theora/parameter_descriptions
/camera/depth_registered/sw_registered/image_rect/theora/parameter_updates
/camera/depth_registered/sw_registered/image_rect_raw
/camera/depth_registered/sw_registered/image_rect_raw/compressed
/camera/depth_registered/sw_registered/image_rect_raw/compressed/parameter_descriptions
/camera/depth_registered/sw_registered/image_rect_raw/compressed/parameter_updates
/camera/depth_registered/sw_registered/image_rect_raw/compressedDepth
/camera/depth_registered/sw_registered/image_rect_raw/compressedDepth/parameter_descriptions
/camera/depth_registered/sw_registered/image_rect_raw/compressedDepth/parameter_updates
/camera/depth_registered/sw_registered/image_rect_raw/theora
/camera/depth_registered/sw_registered/image_rect_raw/theora/parameter_descriptions
/camera/depth_registered/sw_registered/image_rect_raw/theora/parameter_updates
/camera/driver/parameter_descriptions
/camera/driver/parameter_updates
/camera/ir/camera_info
/camera/ir/image
/camera/ir/image/compressed
/camera/ir/image/compressed/parameter_descriptions
/camera/ir/image/compressed/parameter_updates
/camera/ir/image/compressedDepth
/camera/ir/image/compressedDepth/parameter_descriptions
/camera/ir/image/compressedDepth/parameter_updates
/camera/ir/image/theora
/camera/ir/image/theora/parameter_descriptions
/camera/ir/image/theora/parameter_updates
/camera/projector/camera_info
/camera/rgb/image_rect_color
/camera/rgb/image_rect_color/compressed
/camera/rgb/image_rect_color/compressed/parameter_descriptions
/camera/rgb/image_rect_color/compressed/parameter_updates
/camera/rgb/image_rect_color/compressedDepth
/camera/rgb/image_rect_color/compressedDepth/parameter_descriptions
/camera/rgb/image_rect_color/compressedDepth/parameter_updates
/camera/rgb/image_rect_color/theora
/camera/rgb/image_rect_color/theora/parameter_descriptions
/camera/rgb/image_rect_color/theora/parameter_updates
/camera/rgb_rectify_color/parameter_descriptions
/camera/rgb_rectify_color/parameter_updates
/rosout
/rosout_agg
/tf
/tf_static
```

<!-- ## ASUS XTION ORB SLAM2

The Asus Xtion pro does not provide an rgb mono camera image needed by the ORB SLAM2. 

==> STOP DEVEL WITH THIS.

==> USE STEREO INSTEED -->