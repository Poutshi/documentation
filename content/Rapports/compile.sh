# give it the main.txt file that contains the chapters in the order you want 
# path to files should be relative to the location of the main.txt
# ex : cat main.txt
# simulators.md
# UAV_simulation.md
# rotors.md
# qgc.md
# mavlink.md

#path=$(dirname "$i")
#basename=$(basename ${i%.*} )


STYLES="-c styles/latex.css \
        -c styles/my_pandoc.css \
        -c styles/pandoc.css \
        -c styles/pandoc-solarized.css \
        -c styles/tufte.css \
        -c styles/tufte-extra.css"

FILTERS="--filter pandoc-citeproc \
            --filter graphviz.py \
            --filter pandoc-fignos\
            "

options="--verbose -s \
            --toc  --self-contained  \
            --resource-path=.:../../static/media/ \
            --bibliography biblio.bib \
           --from markdown+tex_math_single_backslash \
            "

#--bash-completion --metadata pagetitle="..."

opt_pdf="--verbose -s \
        --toc  --self-contained  \
        --resource-path=.:../../static/media/ \
        --bibliography biblio.bib "


# html5 
# STYLES=" "
echo "pandoc $options $FILTERS  $STYLES -o out.html file.md"

pandoc $options $FILTERS $STYLES --to html5 -o Simulation.html \
Simulation/simulators.md Simulation/UAV_simulation.md Simulation/rotors.md Simulation/qgc.md Simulation/mavlink.md Simulation/PX4_dev.md \
Simulation/sitl.md Simulation/sitl_ros.md offboard/offboard.md offboard/offboard_sim.md \
Annexes/front.md Annexes/quad.md Annexes/ids.md Annexes/econhexcam.md Annexes/serial.md Annexes/v4l2webcam.md \
biblio.md \
Simulation/front.yaml ; 

pandoc $options $FILTERS $STYLES --to html5 -o obstacle_avoidance.html \
avoidance/1.Obstacle_avoidance_intro.md avoidance/2.mapping.md  \
avoidance/3.Octomap_ros.md \
avoidance/4.global_planner_pkg.md \
avoidance/5.Visual_odometry_biblio.md \
avoidance/Annexes/*.md \
biblio.md \
avoidance/front.yaml;

# pandoc $options --to html5+smart -o Annexes.html \
# Annexes/quad.md Annexes/ids.md Annexes/econhexcam.md Annexes/serial.md Annexes/v4l2webcam.md


# #pdf

pandoc $opt_pdf --pdf-engine=pdflatex -o Simulation.pdf \
Simulation/simulators.md Simulation/UAV_simulation.md Simulation/rotors.md Simulation/qgc.md Simulation/mavlink.md \
Simulation/sitl.md Simulation/sitl_ros.md \
offboard/offboard.md offboard/offboard_sim.md \
biblio.md Simulation/front.yaml ; 

pandoc $options $FILTERS $STYLES --pdf-engine=pdflatex --to latex -o obstacle_avoidance.pdf \
avoidance/1.Obstacle_avoidance_intro.md avoidance/2.mapping.md  \
avoidance/3.Octomap_ros.md \
avoidance/4.global_planner_pkg.md \
avoidance/5.Visual_odometry_biblio.md \
avoidance/Annexes/*.md \
biblio.md \
avoidance/front.yaml;