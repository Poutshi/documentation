# Visual Landing Help

## Library

### Compile

``` 
required : OpenCv
cd ~/workspace/SKYWAYS.prj/ComputerVision/Landing_help/AH_circle/Library
mkdir build
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local ../src
make -j4
sudo make install

```

### Unitary test

#### Compile

    cd ~/workspace/SAA.prj/VISION/landing_video_test
    mkdir build
    cmake ..
    make -j4

#### Launch

    cd ~/workspace/SAA.prj/VISION/landing_video_test/build
    ./Landig_video_test

## Use with ROS

The metapackage is named : visual\\\_landing\\\_ros

It contains two packages :

\* **visual\\\_landing\\\_control** : Package to control the landing
help trajectory with the Pixhawk using the PX4 stack firmware \*
**visual\\\_landing\\\_pose** : Airbus helicopter visual landing
package.

### Visual landing pose package

#### Subscribe

\[default\] the package doesn't subscribe to a topic. Image capture is
directly made by the node. \[optional \] subscribes to any topic that
publishes an image. To use this features comment the preprocessor
'NOSUB'

#### Publish

\* geometry\\\_msgs::Pose pose : The landing pad position. The
orientation is set to zero. The message is published only if something
is detected. \* geometry\\\_msgs::Point barycenter : The center of the
landing pad in pixels. \* sensor\\\_msgs::ImagePtr image : The result
image with barycenter, detected circles and estimated pose.

### Visual landing control package

#### Subscribe

#### Publish

[Vilahe Gazebo](/visual_landing_help/simulation)
