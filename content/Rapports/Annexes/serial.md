## Serial connection

It is possible to connect to the Pixhawk via serial port using either
ROS or pure c code to launch  commands.

## ROS

![Link between MAVLink and ROS](fcu/mavlink_mavros.jpg)

## Pure c

Download the **_c\_uart\_interface\_example_** from [MAVLink Github deposit](https://github.com/mavlink/c_uart_interface_example)

To run the code use `mavlink_control -d /dev/ttyUSB0 -b 921600`

The code uses mainly the MAVLink command
[SET\_POSITION\_TARGET\_LOCAL\_NED](http://mavlink.org/messages/common#SET_POSITION_TARGET_LOCAL_NED)


*******************************