## IDS Camera

![camera ueye](sensors/camera/ueye.jpg)

Install the [IDS uEye SDK](https://en.ids-imaging.com/download-ueye-emb-hardfloat.html),

Check if you have a hard float of soft float embedded
system first and be careful to install the dependencies for the SDK
specified in the PDF file.

********************
