## UAVs and motor rotation

<!-- ### quadcopter -->

 * ![quad](uav/quad.jpg){width=300px}

<!-- ### Motor Rotation -->

 * ![motor rotation for quadcopters](uav/quad_rotation.jpg){width=200px}

<!-- ### Hexacopter -->

![Hexacopter DJI F550](uav/hexa-f550.jpeg){width=300px}

<!-- ### Motor rotation  -->

![F550 motor rotation](uav/motororder-hexa-x-2d.png){width=200px}

************