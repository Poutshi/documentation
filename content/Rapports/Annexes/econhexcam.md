## econ Systems : CU135 camera

![camera econ usb3](sensors/camera/econ.png)

The CU135 allows to record all the way up to 4k video at 15 fps or
640*480 at 120 fps.

## Usage with ROS

In order to use this camera with ROS you must download the
*cv_camera* package.

## Capturing from the e-conHEXCUTX

### Preparing the buffers

``` cpp
struct  v4l2_buffer camera_buffer[MAX_CAM]; // v4l2 camera buffers  
for (cam = 0; cam < MAX_CAM ; cam++) {
  camera_buffer[cam].type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  camera_buffer[cam].memory = V4L2_MEMORY_USERPTR;
}
```

### Opening cameras and start stream

  - [Opening cameras and start stream](/birdeyeview/econhexcam/v4l2webcam) 

### Memory allocation

```cpp
    fbuffer1 = (unsigned char *) malloc((sizeof(unsigned char)) * (buffer_size * 3));
    if (! fbuffer1) {
        fprintf(stderr,"malloc: %s \n", strerror(errno));
        exit(-errno);
    }
    fbuffer2 = (unsigned char *) malloc((sizeof(unsigned char)) * (buffer_size * 3));
    if (! fbuffer2) {
        fprintf(stderr,"malloc: %s \n", strerror(errno));
        exit(-errno);
    }
    fullbuffer = (unsigned char *) malloc((sizeof(unsigned char)) * (img_size * 3 * 2));
    if (! fullbuffer) {
        fprintf(stderr,"malloc: %s \n", strerror(errno));
        exit(-errno);
    }
    imagebuffer1 = (unsigned char *) malloc((sizeof(unsigned char)) * (img_size * 3));
    if (! imagebuffer1) {
        fprintf(stderr,"malloc: %s \n", strerror(errno));
        exit(-errno);
    }
    imagebuffer2 = (unsigned char *) malloc((sizeof(unsigned char)) * (img_size * 3));
    if (! imagebuffer1) {
        fprintf(stderr,"malloc: %s \n", strerror(errno));
        exit(-errno);
    }


```

### Buffer Queue and dequeue

``` cpp
for (i = 0 ; i < CAPTURE_MAX_BUFFER ; i++) {
  for (cam = 0; cam < MAX_CAM ; cam++) {
    if ((num_cam & (1 << cam)) && (dq_err & (1 << cam))) {  
      v4l2_capture_dq_v4l2_buffer(app_data->camera_dev[cam], camera_buffer + cam);
      v4l2_capture_q_v4l2_buffer(app_data->camera_dev[cam], camera_buffer + cam);
    }
  }
}

```

<!-- ## Main loop for image capture

## Close and release buffers -->

*******************