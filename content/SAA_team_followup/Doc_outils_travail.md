Bonjour,

Point essentiels avant de lire la suite : 

* Je parle dans ce document en mon nom uniquement. 
* Le points discutés ci-après s'appliquent à l'équipe SAA. Je considère que cela peut bénéficier aux autres mais je ne saurai parlé à leur place. 

## Documentations et User Manuals

### Quantité et densité des documents

Je commence tout d'abord en répondant tout de suite au problème de documentation. Je reconnais qu'il y a peu de documents à disposition des autres équipes et de l'équipe managériale. Je récuse ici l'idée que le manque de documentation soit essentiellement du à une paresse de l'équipe ou à une volonté de dissimuler des informations. 

La légéreté de la documentation est du avant tout au type de POC que nous avons mis en place avant 2019. Ces travaux ont subi un grand nombre de modifications et nous avons souvent changé de sujets. Cette instabilité au niveau des sujets mais aussi au niveau des personnes fait que les documents n'ont jamais pu être rédigé sereinement. 

Cela étant dit, je le répéte, je suis tout à fait conscient qu'il nous faut améliorer nos documents, rédiger nos architectures et les partager avec les mangers. 

### User Manuals

Concernant les documents déjà existants:  L'équipe Sens and Avoid rédige un README ou un User Manual (selon les envies de chacuns) devant chaque projet en cours de developpement. Nous souhaitons continuer à utiliser ce process.

 * Ce document est hébergé dans un git. 
* Assez souvent les depots git sont téléverser dans un Gitlab. 
* <mark> A ce jour nous utilisons nos gitlab perso. Est-il nécessaire d'avoir un Gitlab commun ? je pose la question</mark>
### Hebergement de la documentation

- La proposition d'héberger les documents et les softwares dans un reseau Airbus type fdat se concoit parfaitement à échelle de temps moyenne. Nous acceptons d'alimenter ce réseau une à deux fois par mois. 
- Alimenter fdat à une fréquence supérieure à deux fois par mois est contre productive et chronophage. Nos outils de travail sont déconnectés du réseau Airbus et les aller retour entre les réseaux sera un frein à l'avancement.
- Un serveur type fdat n'est pas approprié au stockage des codes sources. Nos source sont et seront hébérgé dans un serveur de versionning type *GIT* . Le serveur fdat contiendra des documents avec des pointeurs vers le serveur git. Nous veillerons à garder ces serveurs toujours à jour pour faciliter le téléchargement des softwares. 

## Les réunions de suivi

Nous avons mis en place un rythme de réunion qui nous permet de plannifier nos activités de manière fine

* Une réunion d'équipe par semaine dans laquelle nous discutons des activités à une semaine où nous consignons 
   * les activités faites
   * les activités à faire
   * Les taches réussies, les echecs, les taches postponed. 
 * Une fois par mois lors de la réunion d'équipe  nous discutons des tâches à moyen terme à horizon 3 mois.
 * Une fois par an ou deux fois par an nous discutons des activités à l'année. 
 * <mark>Une réunion avec tout le lab serait aussi la bienvenue. Aujourd'hui elle n'a plus lieu. Cette réunion ne peut pas se faire toutes les semaines mais plutôt tous les mois. Au même rythme que la réunion mensuelle des équipes</mark>

## Outils de plannification

A ce jour nous avons rédigé un gantt fait sous forme de pdf non modifiable. Une première tentative fut d'utiliser le logiciel TASKS de Google pour suivre l'évolution des taches. 

* Aujourd'hui il y a un besoin d'utiliser un logiciel plus approprié à la gestion de projet : Redmine, Jira, Gitlab ... 
  * <mark>  Ce point est à discuter plus en profondeur </mark>
  * Apparement openProject répond au besoin. 
