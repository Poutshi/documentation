# OFFBOARD

## Prerequisites

  - [ROS](/software/ros) installed
  - [QGroundControl](/software/qgc) installed
  - Download [MAVROS
    example](https://dev.px4.io/en/ros/mavros_offboard.html) or use our
    code

The offboard mode is used to send mavlink command via the companion
computer.

\<WRAP important\> Passing to offboard mode requires to **send positions
messages** and to **continue sending** those messages to keep the
connection alive. \</WRAP\>

![](/fcu/connection_graph.png)

## Launching Offboard Mode

In order to be able to control the offboard mode with a switch on the
radio controller it needs to be configured in
[QGroundControl](/software/qgc) Two setups are possible :

  - Use offboard [switch](/fcu/offboard#Offboard%20switch) to override
    current mode
  - Use offboard in the list of
    [modes](/fcu/offboard#Offboard%20Flight%20Mode)
  - Use [software](/fcu/offboard#Software%20offboard%20trigger) to ask
    for offboard mode 

### Offboard Switch

The offboard switch is a function that when toggled the offboard mode
overrides any running mode. Don't forget to set the
$\\texttt{RC\_OFFB\_TH}$, this parameter allows to set when a switch
detects the high position (from 0 to 1, if the switch has 3 positions
put above 0.5 to use only the last position). When the switch is flipped
again the fcu goes back to the mode given by the flight mode channel (or
base mode if a flight mode channel was not set).

### Offboard Flight Mode

Offboard can be configured like any flight mode (for example : altitude,
stabilized, ...) but in order to not be rejected it needs the sending of
position via MAVLink. If the position are not sent the previous flight
mode stays on.

### Our Setup

In order to be safe we use both of the solutions proposed above and map
them onto the channel 6. The $\\texttt{RC\_OFFB\_TH}$ was put to 0.7.

![](/fcu/radio_setup.png)

| Channel 6  | Flight Mode |
| ---------- | ----------- |
| Position 1 | Altitude    |
| Position 2 | Stabilized  |
| Position 3 | Offboard    |

This corresponds to the following setup on the radio command :

![](/fcu/radio.png)

### Software Offboard Trigger

Using ROS or c code the offboard mode can be triggered overriding any
currently used flight mode. This methods is using the
[MAVLink](/fcu/serial/mavlink) $\\texttt{COMMAND\_LONG}$

\<WRAP info\> This is considered dangerous and therefore is not advised
\</WRAP\>

##### Lost connection

If the offboard connection a failsafe is launched to be programmed in
the QGroundControl parameters. (Two failsafe are available one if the RC
fails too $\\texttt{COM\_OBL\_ACT}$ and one if the RC is still
accessible $\\texttt{COM\_OBL\_RC\_ACT}$)

## Offboard Commands

Once the offboard connection is established we can start sending
commands, in our case we publish command which can take the form of
either a x,y,z position or vx,vy,vz velocity.

\<WRAP important\> Don't forget to do **ros.spinOnce()** to refresh the
**publish** of pose/velocity and the **subscribed** topic to have update
of the current position of the drone \</WRAP\>
