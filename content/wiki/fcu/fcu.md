# FCU : Flight Control Unit

The FCU is the part of the system commanding the [uav](uav)
[motors](/hardware/motor).

Using a [serial connection](/fcu/serial) we communicate with the
[Pixhawk](/fcu/pix) in [OFFBOARD mode](/fcu/offboard).
