# MAVLink

## Presentation

MAVLink is the Micro Air Vehicle Link, it is a protocol with header-only
messages (meaning all the info for communication is in the header of the
message then the payload is sent) that allows communication with a UAV,
through a set of messages defined in XML. There are two version of
MAVLink v1.0 and v2.0, the main difference between them is that the
second version is able to have more secure communication.

To facilitate the use of MAVLink a c/c++ library was created. Inside we
find encode and decode function as well as type declaration to help
handle the different information. The message are stored in
mavlink/include/mavlink/v1.0/common/ with the format mavlink msgs name
of message.h

\<wrap info\>Don’t hesitate to look at them to get the order of
parameter in the function encode(pack)/decode. Pack is like encode
except the header part of the message is already done. \</wrap\>

In a c++ project you can download the .h files from : 1. For v1.0 :
[Library v1](https://github.com/mavlink/c_library_v1) 2. For v2.0 :
[Library v2](https://github.com/mavlink/c_library_v2) Then you just have
to include mavlink.h in common to have all the files.

\<wrap info\>Don’t forget to add MAVLink to our include path.\</wrap\>

We are going to see two example codes $\\texttt{c\_uart\_example}$ and
$\\texttt{mavlink\_udp}$

## c\_uart\_example

### Download and use

Download the $\\texttt{c\_uart\_interface\_example}$ from MAVLink Github
deposit. <https://github.com/mavlink/c_uart_interface_example>

To run the code use `mavlink_control -d /dev/ttyUSB0 -b 921600`

The code uses mainly the MAVLink command
[SET\_POSITION\_TARGET\_LOCAL\_NED](http://mavlink.org/messages/common#SET_POSITION_TARGET_LOCAL_NED)

### Brief explanation

The purpose of this example is to show how to send command to the
[FCU](/fcu/) using a serial port. This code is split in 3 main classes :

  - `serial port` : Opens the serial port of the companion computer and
    uses it to send the packets of the MAVLink commands.
  - `autopilot interface` : Interprets the commands to be send and
    converts them to binary message using "mavlink.h". **Contains read
    and write thread.**
  - `mavlink control` : File with teh main code and the list of commands
    to be launched.

## mavlink\_udp

This code enables to emulate the communication from the [FCU](/fcu) to
the [QGC](/software/qgc)

[mavlink udp
example](https://github.com/mavlink/mavlink/tree/master/examples/linux)

By default the port used by this code are the same as the [PX4
SITL](/simulation/sitl/sitl/px4_sim). In order to force
[QGC](/software/qgc) to recognize the messages send to it via UDP as a
real [FCU](/fcu) we need to set the component id to 1 instead of 200.
