\======= MQTT =======

MQTT (Message Queuing Telemetry Transport) is message protocol, since it
is based on the publish-subscribe archetype it is simple to implement
