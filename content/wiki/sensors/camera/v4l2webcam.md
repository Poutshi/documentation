# Streaming using a webcam

## Common steps of a v4l2 application

1.  Open a descriptor to the device.
2.  Retrieve and anlyse the device's capabilities. 
3.  Set the capture format. 
4.  Prepare the device for buffer handling. When capturing a frame, you
    have to submit a buffer to the device (queue), and retrieve it once
    it’s been filled with data (dequeue). However, before you can do
    this, you must inform the device about your buffers (buffer
    request).
5.  For each buffer you wish to use, you must negotiate characteristics
    with the device about you buffers and cereate a new memory mapping
    for it. 
6.  Put the device into the streaming mode.
7.  Once your buffers are ready, all you have queueing/dequeuing your
    buffers repeatedly, and every call will bring you a new frame. The
    delay you set between each frames by putting your program to sleep
    is what determines your FPS (frames per second) rate.
8.  Turn off streaming mode.
9.  Close your descriptor to the device.

## Openning and closing device

``` cpp
int main (){
  int fd; 
  if ((fd = open("/dev/video1", O_RDWR))<0){
    perror("open");
    exit(1);
  }
  
  // .....
  close(fd);
  return EXIT_SUCCESS;
}
```

# V4L helper functions

## Opening cameras and start stream

``` cpp
 num_cam = v4l2_start_stream(app_data, cmdline);
 if (num_cam == 0) {
 printf("\n\nNO CAMERA NODES PRESENT\n\n");
   free(app_data);
   app_data = NULL;
   return 0;
}
cameras_connected = num_cam;

```

### v4l2 start stream

* **Prototype**

``` cpp
int v4l2_start_stream(struct app_data *app_data, struct cmdline_args cmdline);
```

* **Implementation**

```cpp
int v4l2_start_stream(struct app_data *app_data, struct cmdline_args cmdline)
{
    char *dev_node = (char *) malloc((sizeof(char)) * 16);
    int cam = 0, num_cam = 0;
    for (cam = 0; cam < MAX_CAM; cam++) {
        sprintf(dev_node, DEV_NODE, cam);
        app_data->camera_dev[cam] = init_v4l2_capture(dev_node, cmdline.width, cmdline.height, UYVY);
        if (app_data->camera_dev[cam] != NULL)
            num_cam |= (1 << cam);
    }
    free(dev_node);

    if (num_cam == 0)
        return 0;
    else {
        for (cam = 0; cam < MAX_CAM; cam++) {
            if (num_cam & (1 << cam)) {
                v4l2_capture_start(app_data->camera_dev[cam]);
            }
        }
    }
    return num_cam;
}
```

 * Callees

```cpp
init_v4l2_capture (for MAX CAMERA) 
v4l2_capture_start()
```

#### Capture initalization

\* Prototype

```cpp
v4l2_capture_t *init_v4l2_capture(const char *filename, uint32_t width, uint32_t height, uint32_t pix_fmt);
```

**Actions**:

  - Create a `%%v4l2_capture_t%%` device using malloc. 

<!-- end list -->

  - Open the camera device using the filename. Typically:
    `"/dev/video(n)"` 
      - This is done with the system I/O function: `%%int open (const
        char *__file, int __oflag, ...)%%`
      - The open function returns a file descriptor strored in
        ''capture\_dev-\>fd '' 

<!-- end list -->

  - Enumerate frame size `%%ioctl(capture_dev->fd,
    VIDIOC_ENUM_FRAMESIZES, (&frmsize)) %%`
  - Check device capabilities '' %%ioctl(capture\_dev-\>fd,
    VIDIOC\_QUERYCAP, \\\&capability) %%'' 
  - Set format info 
  - Set capture sync
  - Allocate capture buffers:`%% int
    alloc_capture_buffers_userptr(v4l2_capture_t *capture_dev) %%` 
    1.  Allocate request buffers: `%% memset reqbuf of type
        v4l2_requestbuffers %%`
    2.  Allocate v4l buffer: `%% memset buf of type v4l2_buffer %%`.
    3.  `%% reqbuf.count = MAX_BUFFER = 4%%`.
    4.  `%% reqbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE%%`.
    5.  `%% reqbuf.memory = V4L2_MEMORY_USERPTR%%`.
    6.  `%% ioctl(capture_dev->fd, VIDIOC_REQBUFS, &reqbuf)%%`.
  - Set loop state value to : `%% V4L2_CAPTURE_LOOP_PAUSE %%`
  - return `%%capture_dev%%`

#### Starting capture

This function sets the `%%VIDIOC_STREAMON%%`.

\* **Prototype**

``` cpp
int v4l2_capture_start(v4l2_capture_t *capture_dev)
```

\* **Implementation**

``` cpp
int v4l2_capture_start(v4l2_capture_t *capture_dev)
{
    int buffer_type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (ioctl(capture_dev->fd, VIDIOC_STREAMON, &buffer_type) < 0) {
        ERROR("VIDIOC_STREAMON: %s\n", strerror(errno));
    }
    return 0;
}
```