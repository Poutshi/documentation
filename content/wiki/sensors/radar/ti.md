# Texas Instrument IWR1642 Radar

The TI radar uses a bandwidth of chirp between 76GHz and 81GHz. The
large bandwidth allows for much better resolution but the more the
resolution is increased the smaller the maximum detection distance
becomes.

![TI Radar](/sensors/radar/radar_ti.jpg)
