# RADAR

The RADARs considered in this project :

1.  [IMST sentire sR-1200 Series Radar Module](/sensors/radar/imst)
2.  [Texas Instrument Radar : IWR1642](/sensors/radar/ti)

# Possible uses

\* Landing help \* C3 Detections of buildings in bad weather (with IMST
Radar) \* Object speed detection \* Very precise small range detection
(with TI radar)
