# IMST sentire sR-1200 Series Radar Module

![](/sensors/radar/fmcw-radar.png)

This is a Frequency Modulated Continuous Wave (FMCW) Radar which send a
chirp to retrieve the distance of a target. A code was made in ROS in
order to launch and record the information from the RADAR.

## Important technical informations

| Characteristic      | Value                       |
| ------------------- | --------------------------- |
| Antenna FOV         | 65° Azimuth, 24° Elevation  |
| Operating Frequency | 24GHz - 24,25GHz (ISM Band) |
| Operating Voltage   | 10.5V - 13V                 |
| Operating Power     | 4.5W                        |
| Weight              | 280g                        |

## How to launch

This launch file starts a few nodes :

  - Radar communication
  - Camera
  - Recorder

The launch file to launch all the nodes : ![Launch
File](/sensors/radar/radar_image.launch)

    roslaunch ros_radar_demo radar_image.launch

It is possible also to add a preview parameter in order to have the view
of the camera during the measurements. `roslaunch ros_radar_demo
radar_image.launch preview:=1`

To start recording in a rosbag :

    rosservice call /record/cmd "record"

To stop recording :

    rosservice call /record/cmd "stop"

## Must read documentation

![Command Interface](/sensors/radar/sr1200e_command_interface.pdf) : How
the communication with the radar is done

![User Manual](/sensors/radar/sr1200e_development_kit.pdf) :
Explanations of the radar functions and how to use Sentool to test it.
