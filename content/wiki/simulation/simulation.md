# Simulation

[main](/simulation/main)  
[SITL](/simulation/sitl/sitl)

  - [SITL introduction](/simulation/sitl/sitl)
  - [PX4 Simulation](/simulation/sitl/sitl/px4_sim)
  - [ROS plugins](/simulation/sitl/sitl/px4_ros)
  - [Sitl Gazebo](/simulation/sitl/sitl/sitl_gazebo)
  - [Separate firmware and gazebo](/simulation/sitl/sitl/separate)

[RotorS simulation](/simulation/rotors)
