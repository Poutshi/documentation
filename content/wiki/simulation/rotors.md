# RotorS simulation

``` 
@incollection{furrer2016rotors,
  title={Rotors—A modular gazebo mav simulator framework},
  author={Furrer, Fadri and Burri, Michael and Achtelik, Markus and Siegwart, Roland},
  booktitle={Robot Operating System (ROS)},
  pages={595--625},
  year={2016},
  publisher={Springer}
}

```

## Simulated parts

  - Body 
  - Motors 
  - Aerodynamic effects
  - Sensors : IMU, odometry, visual inertial sensor, user developed
    sensors. 
  - Simple controller : Geometric Controller (cf Geometric tracking
    control of a quadrotor UAV on SE (3).)
  - State estimation : position, orientation, linear and angular
    velocity.
      - Ideal estimator given by a Gazebo plugin. 
      - Multi Sensor Fusion (MSF) framework (( [A robust and modular
        multi-sensor fusion approach applied to mav
        navigation](https://www.research-collection.ethz.ch/bitstream/handle/20.500.11850/79512/eth-7648-01.pdf?sequence=1)
        ))

![](/simulation/buildingblock.png)

## Controller

![](/simulation/controller.png)

## State estimation

IMU : attitude measurement. For pose estimation use : vision, laser,
maps, SLAM, Vicon, marker based localisation.

  - *Stephan Weiss. Vision based navigation for micro helicopters. PhD
    thesis, ETH Zurich, 2012.*
  - *Markus W. Achtelik. Advanced Closed Loop Visual Navigation for
    MicroAerial Vehicles. PhD thesis, ETH Zurich, 2014.*
  - *N. Trawny and S. I. Roumeliotis. Indirect Kalman filter for 3D
    attitude estimation. Technical Report 2005-002, University of
    Minnesota, Dept. of Computer Science and Engineering, 2005.*

## Simulator overview

![](/simulation/sim_overview.png)

| Sensor   | Description                                                                                                                                                                                                                                                                                                                           | Xacro-name Gazebo plugin(s)                                    |
| -------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------- |
| Camera   | A configurable standard ROS camera                                                                                                                                                                                                                                                                                                    | camera\_macro libgazebo\_ros\_camera.so                        |
| IMU      | This is an IMU implementation, with zero mean white Gaussian noise and a random walk on all measurements as described in (12) and (13)                                                                                                                                                                                                | imu\_plugin\_macro librotors\_gazebo\_imu\_plugin.so           |
| Odometry | Simulate position, pose, and odometry sensors. A binary image can be attached to specify where the sensor works in the world, in order to simulate sensor outages. White pixel: sensor works, black: sensor does not work. It mimics any generic on- or off-board tracking system such as Global Positioning System (GPS), Vicon, etc | odometry\_plugin\_macro librotors\_gazebo\_odometry\_plugin.so |

## Using the simulator

Install the rotors\_simulator package Install the dependencies :
Mavlink, octomap (optional), joy(optional)

    $  sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu `lsb_release 
    
    $  sudo apt-get install ros-kinetic-joy ros-kinetic-octomap-ros ros-kinetic-mavlink python-wstool python-catkin-tools protobuf-compiler libgoogle-glog-dev ros-kinetic-control-toolbox
    
    $  sudo rosdep init
    $  rosdep update
    $  source /opt/ros/kinetic/setup.bash
    
    $  mkdir -p ~/catkin_ws/src
    $  cd ~/catkin_ws/src
    $  catkin_init_workspace  # initialize your catkin workspace
    $  wstool init
    
    $  wget https://raw.githubusercontent.com/ethz-asl/rotors_simulator/master/rotors_hil.rosinstall
    $  wstool merge rotors_hil.rosinstall
    $  wstool update

    $  cd ~/catkin_ws/
    $  catkin build

### Procedure

1.  Select your model
    1.  Pick one of the models provided by the RostorS, or
    2.  Build your own model
2.  Attach sensors to the MAV

<!-- end list -->

``` 
   - Use one of the Xacro for the sensors shipped with RotorS, or
   - Create your own sensor ((see %%rotors_gazebo_plugins%% for references and http://gazebosim.org/tutorials?cat=sensors )) and attach them directly, or create a Xacro, analogously to the Xacro in components_snippets.xacro
 - Add your controller to your MAV
   - Start one of the controllers shipped with RotorS, or
   - Write your own controller, and launch it.
 - Use a state estimator
   - Do not use a state estimator, and use the output of the ideal odometry sensor directly as an input to your controller, or
   - Use MSF (developped by ETHZ ASL)
   - Use you own state estimator.
```

#### Hovering example

    roslaunch rotors_gazebo mav_hovering_example.launch
