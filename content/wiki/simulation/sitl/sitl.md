# SITL Simulation

## prerequisites

  - ROS (kinetic) 
  - Gazebo7 

\#\# Install script

On a clean system you can use the file

\[ubuntu\_sim.sh\](<https://raw.githubusercontent.com/PX4/Devguide/master/build_scripts/ubuntu_sim.sh>)
: This is a bash script for setting up a ROS/Gazebo development
environment for PX4 on Ubuntu LTS (16.04). It installs the common
dependencies for all targets (including Qt Creator) and the ROS
Kinetic/Gazebo 7 (the default).

\#\#\# Installs:

``` 
  - Common dependencies libraries and tools as defined in `ubuntu_sim_common_deps.sh`
  - ROS Kinetic (including Gazebo7)
  - MAVROS
```

\`$ source ubuntu\_sim.sh\`

\#\# Install Dependencies

\[ubuntu\_sim\_common\_deps.sh\](<https://raw.githubusercontent.com/PX4/Devguide/master/build_scripts/ubuntu_sim_common_deps.sh>)
: This file is a Bash script for setting up a PX4 development
environment on Ubuntu LTS (16.04). It can be used for installing
simulators (only) or for installing the preconditions for Snapdragon
Flight or Raspberry Pi.

\#\#\#Installs:

``` 
  - Common dependencies and tools for all targets (including: Ninja build system, Qt Creator, pyulog)
  - FastRTPS and FastCDR
  - jMAVSim simulator dependencies
  - PX4/Firmware source (to ~/src/Firmware/)
```

    sudo usermod -a -G dialout $USER
    source ubuntu_sim_common_deps.sh
    sudo apt-get remove modemmanager

### Interaction with RotorS Simulator

![sim\_sitl.png](/simulation/sim_sitl.png)
