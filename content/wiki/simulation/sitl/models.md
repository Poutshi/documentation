# Gazebo models

The simulation contained in the firmware repository
\`<https://github.com/PX4/Firmware>\` depends on the sitl\\\_gazebo
submodule \`<https://github.com/PX4/sitl_gazebo>\`

We can separate the sitl launch from the simulation files. This way we
have a better understanding of what's happening when we launch the
global simulation with the following command :

\`make posix\_sitl\_default gazebo\`

Moreover, having two separate directories make it easier to interface
with another flight controller than the PX4.

## SITL PX4 firmware

The simulated PX4 firware is located in \[1\] :

\`\~/src/Firmware/build/posix\_sitl\_default/px4\`

### Execute PX4 Firmware

#### Usage

``` 
./px4 [-d] [data_directory] startup_config [-h]
   -d            - Optional flag to run the app in daemon mode and does not listen for user input.
                   This is needed if px4 is intended to be run as a upstart job on linux
<data_directory> - directory where ROMFS and posix-configs are located (if not given, CWD is used)
<startup_config> - config file for starting/stopping px4 modules
   -h            - help/usage information

```

#### Launch demo

    cd ~/src/Firmware/posix-configs
    ./../build/posix_sitl_default/px4 SITL/init/ekf2/iris

\`SITL/init/ekf2/iris\` : is a config file specific to an Iris
quadcopter using ekf2 as a pose estimator.

## SITL Models

Models are based on [PX4
SITL\_Gazebo](https://github.com/PX4/sitl_gazebo) repository.

If you donwload the [PX4 Firmware](https://github.com/PX4/Firmware) the
sitl folder is to find in :

\`src/Firmware/Tools/sitl\_gazebo\`

To separate things up we downloaded the sitl repo in a different
location.

\`\~/workspace/SAA.prj/SIMULATION/sitl\_gazebo\`

All the modifications are made directly to this directory.

### Build the gazebo plugins

Compiling using mavlink v2.0 didn't work. It is advised to use the
Mavlink v1.0

    mkdir -p Build 
    cmake -D_MAVLINK_INCLUDE_DIR=/opt/ros/kinetic/include/ -DCMAKE_INSTALL_PREFIX=~/workspace/SAA.prj/SIMULATION/sitl_gazebo/install .. 
    make -j3

### Launch demo

\`gazebo worlds/iris.world \`

Connection is automatically established between the simulator and the
firmware.

### Dive into the structure

## Drone with Lidar

### F550 and Scanse

1.  we suppose that the Firmware is cloned into \`\~/src/Firmware\`
