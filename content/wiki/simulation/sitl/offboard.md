## Offboard Control

1.  Launch the simulation 

<!-- end list -->

``` 
cd ~/src/Firmware
make posix_sitl_default gazebo 

```

1.  Launch [QGroundControl](/simulation/gazebo#QGroundControl%20Install)
    

\<WRAP important\> Change default GPS location to Airbus Helicopter
Marignane \</WRAP\>

![](/simulation/gz_qgc.png)

1.  Launch PX4 MAVLink communication with ROS

\`roslaunch mavros px4.launch
fcu\_url:="<udp://:14540@127.0.0.1:14557>"\`

1.  Launch offboard demo

\` rosrun px4\_comm offb\_demo\_node\`

![](/simulation/offboard_demo_ls.mp4)

[Shell Offboard](/simulation/sitl/offboard/script)
