## PX4 Simulation

The only slight difference to "normal behaviour" is that ROS initiates
the connection on port 14557, while it is more typical for an offboard
API to listen for connections on UDP port 14540.

![px4\_sim.png](/simulation/px4_sim.png)

### Launch Simulation

### Building SITL Simulation

The build system makes it very easy to build and start PX4 on SITL,
launch a simulator, and connect them. For example, you can launch a SITL
version of PX4 that uses the EKF2 estimator and simulate a plane in
Gazebo with just the following command (provided all the build and
gazebo dependencies are present\!):

    make posix_sitl_ekf2 gazebo_plane

\<WRAP info \> It is also possible to separately build and start SITL
and the various simulators, but this is nowhere near as "turnkey".

\</WRAP\>

The syntax to call \`make\` with a particular configuration and
initialisation file is:

    make [CONFIGURATION_TARGET] [VIEWER_MODEL_DEBUGGER]

where:

  - CONFIGURATION\\\_TARGET : has the format
    \`\[OS\]\[\_PLATFORM\]\[\_FEATURE\]\`
      - OS: posix, nuttx, qurt
      - PLATFORM: SITL (or in principle any platform supported among the
        different OS: bebop, eagle, excelsior, etc.)
      - FEATURE: A particular high level feature - for example which
        estimator to use (ekf2, lpe) or to run tests or simulate using a
        replay.

\<WRAP info \> You can get a list of all available configuration targets
using the command:

\`make list\_config\_targets\` \</WRAP\>

  - VIEWER\\\_MODEL\\\_DEBUGGER: has the format
    \`\[SIMULATOR\]\_\[MODEL\]\[\_DEBUGGER\]\`
      - SIMULATOR: This is the simulator ("viewer") to launch and
        connect: gazebo, jmavsim
      - MODEL: The vehicle model to use (e.g. iris, rover, tailsitter,
        etc). This corresponds to a specific initialisation file that
        will be used to configure PX4. This might define the start up
        for a particular vehicle, or allow simulation of multiple
        vehicles (we explain how to find available init files in the
        next section).
      - DEBUGGER: Debugger to (optionally) use: none, ide, gdb, lldb,
        ddd, valgrind, callgrind. For more information see Simulation
        Debugging.

\<WRAP info\> You can get a list of all available
VIEWER\\\_MODEL\\\_DEBUGGER options using the command: \`make posix
list\_vmd\_make\_targets\` \</WRAP\>

Notes:

  - Most of the values in the \`CONFIGURATION\_TARGET\` and
    \`VIEWER\_MODEL\_DEBUGGER\` have defaults, and are hence optional.
    For example, \`gazebo\` is equivalent to \`gazebo\_iris\` or
    \`gazebo\_iris\_none\`.
  - You can use three underscores if you want to specify a default value
    between two other settings. For example, \`gazebo\_\_\_gdb\` is
    equivalent to \`gazebo\_iris\_gdb\`.
  - You can use a none value for \`VIEWER\_MODEL\_DEBUGGER\` to start
    PX4 and wait for a simulator. For example start PX4 using \`make
    posix\_sitl\_default none\` and jMAVSim using
    \`./Tools/jmavsim\_run.sh\`.

#### Running the default simulation

We suppose that the Pixhawk firmware is cloned in
\`$HOME/$USER/src/Firmware\`

``` 
cd ~/src/Firmware
make posix_sitl_default gazebo

```

This will call the simulation launch file located in
\`Tools.sitl\_run.sh\`. This file usage is as :

    sitl_run.sh rc_script rcS_dir debugger program model src_path build_path

The default simulation has the following arguments :

  - **sitl\\\_bin** or **rc\\\_script** :
    \`\~/Firmware/build/posix\_sitl\_default/px4\`
  - **rcS\\\_dir** : \`posix-configs/SITL/init/ekf2\`
  - **debugger** : none
  - **program** : \`gazebo\`
  - **model** : none
  - **src\\\_path**: \`\~/Firmware\`
  - \*\* build\\\_path\*\*: \`\~/Firmware/build/posix\_sitl\_default\`

A gazebo window should appear containing a quadcopter and a rough
terrain.

![](/simulation/default_gzclient_camera_1_-2018-05-28t12_03_11.317274.jpg)
