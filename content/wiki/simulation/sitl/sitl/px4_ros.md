## Launching Gazebo with ROS Wrappers

The Gazebo simulation can be modified to integrate sensors publishing
directly to ROS topics e.g. the Gazebo ROS laser plugin. To support this
feature, Gazebo must be launched with the appropriate ROS wrappers.

There are ROS launch scripts available to run the simulation wrapped in
ROS:

  - \[posix\_sitl.launch\](<https://github.com/PX4/Firmware/blob/master/launch/posix_sitl.launch>):
    plain SITL launch
  - \[mavros\_posix\_sitl.launch\](<https://github.com/PX4/Firmware/blob/master/launch/mavros_posix_sitl.launch>):
    SITL and MAVROS

To run SITL wrapped in ROS the ROS environment needs to be updated, then
launch as usual:

(optional): only source the catkin workspace if you compiled MAVROS or
other ROS packages from source:

``` bash
cd <Firmware_clone>
make posix_sitl_default gazebo
source ~/catkin_ws/devel/setup.bash    // (optional)
source Tools/setup_gazebo.bash $(pwd) $(pwd)/build/posix_sitl_default
export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:$(pwd)
export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:$(pwd)/Tools/sitl_gazebo
roslaunch px4 posix_sitl.launch
```

Include one of the above mentioned launch files in your own launch file
to run your ROS application in the simulation.

### What's Happening Behind the Scenes

This section shows how the roslaunch instructions provided previously
actually work (you can follow them to manually launch the simulation and
ROS).

First start the simulator using the command below:

``` bash
no_sim=1 make posix_sitl_default gazebo
```

The console will look like this:

``` bash
[init] shell id: 46979166467136
[init] task name: px4

______  __   __    ___
| ___ \ \ \ / /   /   |
| |_/ /  \ V /   / /| |
|  __/   /   \  / /_| |
| |     / /^\ \ \___  |
\_|     \/   \/     |_/

Ready to fly.


INFO  LED::init
729 DevObj::init led
736 Added driver 0x2aba34001080 /dev/led0
INFO  LED::init
742 DevObj::init led
INFO  Not using /dev/ttyACM0 for radio control input. Assuming joystick input via MAVLink.
INFO  Waiting for initial data on UDP. Please start the flight simulator to proceed..
```

Now in a new terminal make sure you will be able to insert the Iris
model through the Gazebo menus, to do this set your environment
variables to include the appropriate sitl\_gazebo folders.

``` bash
cd <Firmware_clone>
source Tools/setup_gazebo.bash $(pwd) $(pwd)/build/posix_sitl_default
```

Now start Gazebo like you would when working with ROS and insert the
Iris quadcopter model. Once the Iris is loaded it will automatically
connect to the px4 app.

``` bash
roslaunch gazebo_ros empty_world.launch world_name:=$(pwd)/Tools/sitl_gazebo/worlds/iris.world
```

Visual Landing Help world

``` bash
roslaunch gazebo_ros empty_world.launch world_name:=$(pwd)/Tools/sitl_gazebo/worlds/iris_nadir_cam.world verbose:=true
```

gazebo\_ros params :

``` bash
extra_gazebo_args
gui
recording
verbose
world_name
```
