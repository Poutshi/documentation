# SITL GAZEBO

This is a flight simulator for multirotors, VTOL and fixed wing. It uses
the motor model and other pieces from the RotorS simulator, but in
contrast to RotorS has no dependency on ROS.

If you want to use the sitl\_gazebo folder in another location than the
default one (inside the Firmware folder) compile it using :

    cd sitl_gazebo
    git submodule update --init --recursive
    mkdir -p build 
    cd build 
    cmake ..
    make -j2

\<WRAP alert\> Use the \`-j\` option with caution. It can lead to an
excessive memory usage crashing the computer \</WRAP\>

## PX4 folder organization

  - **build** : Contains build results
  - **mavlink** : an include directory of mavlink headers.
  - **msg** : protobuf message definition
  - **src** : PX4 flight stack sources.
  - **Tools** : 
      - shell, python scripts
      - jMAVSim, Matlab, Gazebo simulation

## Dependencies

    sudo apt-get install libprotobuf-dev libprotoc-dev protobuf-compiler libeigen3-dev \
    gazebo7 libgazebo7-dev libxml2-utils python-rospkg python-jinja2

## Environment variables

### Basics

    GAZEBO_MODEL_PATH=/usr/share/gazebo-7/models:
    GAZEBO_RESOURCE_PATH=/usr/share/gazebo-7:/usr/share/gazebo_models:
    GAZEBO_MASTER_URI=http://localhost:11345
    GAZEBO_PLUGIN_PATH=/usr/lib/x86_64-linux-gnu/gazebo-7/plugins:
    GAZEBO_MODEL_DATABASE_URI=http://gazebosim.org/models

### SITL setup

in file \`Tools/sitl\_gazebo/setup\_gazebo.sh\`

``` 
SRC_DIR=$1
BUILD_DIR=$2
export GAZEBO_MODEL_PATH=${GAZEBO_MODEL_PATH}:${SRC_DIR}/Tools/sitl_gazebo/models
export GAZEBO_PLUGIN_PATH=${BUILD_DIR}/build_gazebo:${GAZEBO_PLUGIN_PATH}
#export GAZEBO_MODEL_DATABASE_URI=""
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${SRC_DIR}/Tools/sitl_gazebo/Build/msgs/:${BUILD_DIR}/build_gazebo


```

### My ws SITL setup
