# Shell Offboard

New terminal

    cd Firmware
    no_sim=1 make posix_sitl_default gazebo

New terminal

    cd Firmware
    source Tools/setup_gazebo.bash $(pwd) $(pwd)/build/posix_sitl_default
    roslaunch gazebo_ros empty_world.launch world_name:=$(pwd)/Tools/sitl_gazebo/worlds/iris.world

New terminal

    cd catkin_ws
    roslaunch mavros px4.launch fcu_url:="udp://:14540@127.0.0.1:14557"

New terminal

``` 
rosservice call /mavros/cmd/command "{broadcast: false, command: 92, confirmation: 0, param1: 1.0, param2: 0.0, param3: 0.0,
  param4: 0.0, param5: 0.0, param6: 0.0, param7: 0.0}" 
success: True
result: 0

```

New terminal

``` 
rostopic pub  -r10 /mavros/setpoint_raw/local mavros_msgs/PositionTarget "header:
  seq: 0
  stamp: {secs: 0, nsecs: 0}
  frame_id: ''
coordinate_frame: 8
type_mask: 4088
position: {x: 0.0, y: 3.0, z: 3.0}
velocity: {x: 0.0, y: 0, z: 0.0}
acceleration_or_force: {x: 0.0, y: 0.0, z: 0.0}
yaw: 0.0
yaw_rate: 0.0" 

```

New terminal

``` 

```
