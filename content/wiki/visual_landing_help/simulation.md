# Gazebo Simulation

### PX4 Gazebo

    cd <Firmware_clone>
    no_sim=1 make posix_sitl_default gazebo
    source Tools/setup_gazebo.bash $(pwd) $(pwd)/build/posix_sitl_default
    roslaunch gazebo_ros empty_world.launch world_name:=$(pwd)/Tools/sitl_gazebo/worlds/iris_nadir_cam.world
    QGroundControl.AppImage

### Visual landing help

\* In a new terminal : launch the target detection node

    cd workspace/catkin_ws/
    source devel/setup.bash
    roslaunch vilahe_pose visual_landing.launch simulation:=true

\* In a new terminal : launch the node that controls the UAV

``` 
cd workspace/catkin_ws/
source devel/setup.bash
roslaunch vilahe_control visual_landing_control.launch simulation:=true 
```
