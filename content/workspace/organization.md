# Workspace folders 

## catkin_saa_automotive_radar 

Copy of the ros workspace shipped with the Radar.

## catkin_ws

My ros workspace. 

## cuda-workspace

Hexa cam head algorithms 

## Documents

Documentation for the SAA project

## LOGS

Some old PX4 flight logs 

## Pixhawk 

Different versions of the PX4 Firmware.

## SAA.prj 

Non-Ros algorithms for the SAA project. 

## SKYWAYS.prj 

Algorithms of the landing help 

## sweep-sdk 

The SDK of the sweep laser scan. 

## Videos 

Videos related to the SAA. 


## Airbus folders 

### Biblio 
	De la biblio générale commune à tous les sujets et projets. 

### Skyways 

* Doc du projet : SDR, PDR
* Etat d'avancement
* un vieux planning. 
* Le planning (uniquement) d'apprentissage de Gazebo
* Travaux sur le Lidar SF11 avec un servo moteur
* Docs vrac avant que j'arrive. 
* Documentation : Rapport de projet Ali et Ammar

### SAA 

* Uniquement la documentation. Que du texte. 

### Materiel 

* Les datasheets des differents device utilisés. 

