# Reunion hebdomadaire

## Présents 

Clement Chassagne, Clement Bussereau, Ammar Hannachi, Lionel Thomassey, Raphael Fauvet, David Djeffard, Axel Manchon, Ali Karaouzene.  

## Probleme Skyways

Crash Skyway due surement à la batterie qui a coupé à 75° et ils n'ont pas du tenir compte de l'alarme. La deuxieme batterie a coupé vite après. 

## Infos générales 

Les choses à The Camp avancent . Réunion les 23 et 24 juin pour faire un point. 

Des uses cases vont arriver assez vite : 

1. Il nous faut prendre le temps d'écrire. C'est le meilleur support pour montrer notre travail. 
2. Travailler sur la robustesse de nos applis. 

Lionel a rendu la doc à Ammar, Ali et Clement. 

## Discussion sur les documents fournis 

C.C : Faut il se focaliser sur le système entier ou bien sur les blocs. 

LT : Décrire ce qui sera mis en vol puis détailler. 

LT : Il manque la descriptionn technique 

AK : Cette partie sera sur d'autres documents descriptifs de la technique. 

LT : Ok pour cette division. Prenez de votre temps pour faire le document technique. 



### Remarques 

Attention aux termes 

Utilisez les mêmes termes 

Citez les sources 

Lorsque vous citez des sources internes rédiger un passage qui montre que les deux documents sont en phase. 

## MISC

Florent Coste arrive semaine prochaine. 

Réfléchir à ce que fera Florent lorsqu'il sera là : 

* Helico Elec
* ESC sensor et sensorless 
* Dev bloc S pour le PA
* Support sur du bas niveau pour le bloc A 
* Support pour Julien 

## LIDAR 

* Redaction de la doc Turtlebot
* Faire une spec d'integration du Lidar
* Rechercher les SDK qui peuvent nous aider a faire la carte de l'environnement 

## Hardware 

* Mettre en place une methodo pour compiler la veille techno sur le hardware. 

## Devops 

Ali K : Faire un résumé sur ce que propose Eric Schirolla au studio. --> Fait au tableau

## Avenir !

Rediger chacun les risques auxquels on peut faire face si le transfolab devaient "lever des fonds". 

