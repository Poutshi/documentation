# Travaux Collaboration futurs

## XXII 

### Reception 

Nous avons reçu les vidéos ainsi que la base de donnée du premier POC. 

Les résultats contiennent : 

* Suivi de poteaux avec tracking 
* Suivi sans tracking. 

### Pour la suite 

* Revoir la codification couleur. 
  * Rouge opaque pour la zone autour du cable 
  * Dessous Rouge transparent. 
  * Les zones accessibles en vert . 
    * Pas le sol qui doit etre dans sa couleur naturelle. 
  * Garder le code couleur de la derniere video pour les besoins de la presentation. 
  * Proposer deux modes de visualisation : 
    * Avec les zones dangereuses
    * Avec les poteaux et les cables uniquement. 

#### Cas du poteau isolé dans la détection 

Dans le cas où un seul poteau est detecté utiliser des regles pour prédire la position du poteau adjacent même si il n'est pas detecte par le systeme de vision. 

#### Discrimination et catégo des poteaux

Ajouter quelques poteaux (4 ou 5 ) dans la base d'apprentissage et : 

1. Catégoriser les poteaux 

2. Peut on généraliser sur des poteaux jamais appris  grace à 

3. * La position 
   * l'environnemet 
   * ...

   Qu'est ce qui nous permet de generaliser au mieux . 

#### Résultats enrichies 

Position des poteaux dans le repère monde et/ou dans le repère drone. 

Altitude 

Distance des poteaux

Viisualiser le Temps avant impact

#### Display des trajectoires

* Afficher les trajectoires courante du drone et celle extrapolees 
* Afficher la trajectoire qui erlet d'eviter en passant par la zone verte. 

### Embedded

Comment faire tourner l'algo en embarque sur une cible type jetson TX2. 

## The Edge

The Edge nous a fourni trois vidéos de drone détéctés par leur système. 

### Question : 

* Y a t il eu un nouvel apprentissage ? 
* Que contenait la base d'apprentissage du drone 



### Pour la suite 

* Quels sont les distances de detection 
* Detecter aussi loin qu'avec les oiseaux 
* Afficher la distance sur la vidéo 
* Afficher le sens du deplacement 
  * Un degrade vert orange rouge selon la distance 
  * Sens de la vitesse : cercle avec point quand ca vient vers nous et avec croix quand ca s'eloigne. 