# Rencontre avec Matlab sur les sujets automobile 

## Presents

Martin Brochet, Ammar Hannachi, Clement Chassagne, Ali Karaouzene, Raphael Fauvet, Fluvio Martinelli (via skype)

## Presentation de la réunion 

Martin;  *Présetation de la réunion* Fluvio fait le lien entre la R&D et l'industrie automobile. 

Fulvio Martinelli : Ingénieur pilote. But : avoir une collaboration win - win avec les industriels. 

-- Présentation de l'équipe et de la collab -- 

## Présentation Fluvio 

### Plan 

* Object classification 
  * Image classification 
  * Transfer learning
  * Object detection and classification 

* Semantic Segmentation 

* SLAM

* Battery modelisation

### Deep learning 

- Classification 
- Detection 

Presentation des réseaux de neurones convolutionnels. 

Toolbox : Neural Network Toolbox. 

Example de classification avec AlexNet. 

Affichage des différentes phases de pooling, ReLu et activation. 

#### Transfert learning 

Reentrainer un réseau avec de nouvelles catégories sans devoir rejouer tout l'apprentissage depuis le départ. 

* Présentation du "Deep Network Designer" --> Il serait intéressant d'avoir les sorties des couches depuis cette interface graphique. 
  * Il est possible de faire tout type de réseaux tant qu'ils existent dans les supports packages. 
  * Fichier onnx : open neural network exchange. 
  * On peut importer depuis Keras vers matlab. 
  * Un réseau écrit sous Keras, Tensorflow etc .. une fois importé peut être visualisé, reentrainé, utilisé sous Matlab. 
* Le chargement d'image se fait via un data store. Un pointeur vers un endroit ou sont stockées des images dans
  * Un dossier local
  * Une base de données
  * Le cloud
* Execution du reseau
  * Split database
  * trainnetwork
  * Si un GPU existe et la toolbox Parallel computing --> matlab utilise automatiquement les GPU
  * Classify
  * Creer une confusion matrix

#### Object detection 

Train object detector based on groun truth

Deux types d'approches : 

* Machine learning 
  * Aggregate Channel feature 
  * Cascade
* Deep Learning 
  * RCNN
  * Fast RCNN

### Segmentation Semantique 

Segmenter la scene en reprojetant la classif sur l'image originale. 

Besoin d'une labelisation pixel à pixel

Ce sont des réseaux serie basés sur la déconvolution. 

Fonction matlab : semantic seg

Utilisé dans l'automobile pour trouver le "driving space" ou "corridor de navigation".

Utilisé plus pour de la mesure de proximité : Proche, moyen, loin. 

<mark> Sujet à creuser ensemble </mark>

### SLAM 

Robotic Toolbox 

Présentation des méthodes de mapping et de localisation.

Présentation des méthodes de SLAM pour le 2D et le 3D. 

#### Solve Slam 

* UKF

* Particle Filter

* Graph Based : Méthode privilégiée car elle tient le passage à l'échelle. 

#### Path Planning 

* Line, cubic quintic polynomials 
* Spline 
* Grid based : Dijkstra, A étoile
* Probabilistic roadmap 
* RRT 
* RRT star : for smoothing the trajectory. 

#### Path Follower 

* Pure pursuit 
* Predective control : 
  * Rollout all trajectories
    *  Actuator limit 
    * State limits 
    * Motion constraints
  * Assign cost to each trajectory
    * Costmap
    * Obstacles
    * Distance orientation to goal 
    * Distance to path
  * Select best one and execute
    * QP : MPC
    * Trajectory rollout / Dynamic Window

### Lien avec l'automobile 

1. Deep learning and machine learning pour la detection et classification d'objet : Fait principalement par les fournisseurs de capteurs. Black box avec en sortie : objet, vitesse, orientation, distance ... 
2. Semantic segmentation : Fait par les constructeurs dans des voitures sur équipées. 
      * Creer des bases de données d'apprentissage.
      * Faire de la re-simulation.
      * Utilisé pour la certification future des voitures autonomes.
      * Aujourd'hui la segmentation semantique n'est pas embarquée. 
3. Mapping & localisation : 
    * Aucun besoin de créer des maps 
    * Besoin de localisation : Principalement au GPS. 
    * La localisation par GPS est corrigée par de la fusion de données 
    * Pas de monte carlo, UKF ... 
	* Essentiellement basés fusion 
4. Plannification : 
       * RRT star : Pour le confort du passager
5. Path Follower : 
       * Predictif surtout. 

### What about Local maps ? 

C'est plus une question de tracking 

detection --> track manager --> tracking filter --> tracks

Méthode du most important object



## Appréciation générale 

Fluvio est très intéressant. Il faut garder sa carte pour refaire appel à lui il a beaucoup de choses intéressantes à raconter. 

### Workshop 

Workshop sensor fusion à paris le 17 mai

