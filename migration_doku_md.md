# Convert to markdown

Conversion using Pandoc

pandoc file.txt -f dokuwiki -t gfm -s -o file.md
```bash
for i in `find . -name "*.txt"`; do pandoc $i -f dokuwiki -t gfm -s -o  ${i%.txt}.md; done
```
# Finished

 * wiki : suppresed
 * playground : suppresed

## Standalone files 

Have been all either deleted or moved to their parent folder. They mainly consist of local table of contents.

# Folders

* fcu : folder clean. Still need to correct the file from possible pandoc conversion errors.
* sensors :   folder clean. Still need to correct the file from possible pandoc conversion errors.
* simulation : 
* software : folder clean. Still need to correct the file from possible pandoc conversion errors.
* uav :  folder clean. Still need to correct the file from possible pandoc conversion errors.
* visual_landing_help


```bash
.
├── fcu
│   ├── fcu.md
│   ├── offboard.md
│   └── serial.md
├── sensors
│   ├── camera
│   │   ├── econhexcam.md
│   │   ├── ids.md
│   │   ├── v4l2webcam.md
│   │   └── zed.md
│   ├── lidar
│   │   └── main.md
│   └── radar
│       ├── imst.md
│       ├── main.md
│       └── ti.md
├── simulation
│   ├── main.md
│   ├── rotors.md
│   ├── simulation.md
│   ├── sitl
│   │   ├── models.md
│   │   ├── offboard
│   │   │   └── script.md
│   │   ├── offboard.md
│   │   ├── sitl
│   │   │   ├── px4_ros.md
│   │   │   ├── px4_sim.md
│   │   │   └── sitl_gazebo.md
│   │   └── sitl.md
│   └── sitl.md
├── software
│   ├── mavlink.md
│   ├── mqtt.md
│   └── qgc.md
├── uav
│   └── quad.md
└── visual_landing_help
    ├── simulation.md
    └── visual_landing_help.md

```





















