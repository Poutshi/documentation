/**
 * @file offb_node.cpp
 * @brief Offboard control example node, written with MAVROS version 0.19.x, PX4 Pro Flight
 * Stack and tested in Gazebo SITL
 */

#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/State.h>
#include <mavros_msgs/Thrust.h>

mavros_msgs::State current_state;
std::string lastStateReceived;
bool isArm;

void state_cb(const mavros_msgs::State::ConstPtr& msg) {
	current_state = *msg;
    lastStateReceived = msg->mode.c_str();
    isArm = msg->armed;
	ROS_INFO("Callback");


}

int main(int argc, char **argv) {
	ros::init(argc, argv, "offb_demo_node");
	ros::NodeHandle nh;
	std::string state_channel;
	ros::Subscriber state_sub;

	/*ros::Subscriber state_sub = nh.subscribe<mavros_msgs::State>("mavros/state",
			10, state_cb);*/
	state_channel    = nh.resolveName("/mavros/state");
	state_sub        = nh.subscribe(state_channel, 10, state_cb);
	ros::Publisher local_pos_pub = nh.advertise<geometry_msgs::PoseStamped>(
			"mavros/setpoint_position/local", 10);
	ros::ServiceClient arming_client =
			nh.serviceClient<mavros_msgs::CommandBool>("mavros/cmd/arming");
	ros::ServiceClient set_mode_client = nh.serviceClient<mavros_msgs::SetMode>(
			"mavros/set_mode");

	ros::Publisher set_thrust = nh.advertise<mavros_msgs::Thrust>(
			"/mavros/setpoint_attitude/thrust", 10);

	ros::Publisher setattitude_velocity = nh.advertise<geometry_msgs::TwistStamped>(
			"/mavros/setpoint_attitude/cmd_vel", 10);
	ros::Publisher setpoint_velocity =  nh.advertise<geometry_msgs::TwistStamped> (
			"/mavros/setpoint_velocity/cmd_vel",10);

	//the setpoint publishing rate MUST be faster than 2Hz
	ros::Rate rate(20.0);

	// wait for FCU connection
	while (ros::ok() && !current_state.connected) {
		ros::spinOnce();
		rate.sleep();
	}



	geometry_msgs::PoseStamped pose;
	pose.pose.position.x = 0;
	pose.pose.position.y = 0;
	pose.pose.position.z = 0.1;


	geometry_msgs::TwistStamped velocity;
	velocity.twist.linear.x =
			velocity.twist.linear.y =
					velocity.twist.linear.z = 0;
	velocity.twist.angular.x =
			velocity.twist.angular.y =
					velocity.twist.angular.z = 0;

	mavros_msgs::Thrust thrust;
	thrust.thrust = 1000;


	//send a few setpoints before starting
	for (int i = 100; ros::ok() && i > 0; --i) {
		local_pos_pub.publish(pose);
		ros::spinOnce();
		rate.sleep();
	}

	mavros_msgs::SetMode offb_set_mode;
	offb_set_mode.request.custom_mode = "OFFBOARD";

	mavros_msgs::CommandBool arm_cmd;
	arm_cmd.request.value = true;

	ros::Time last_request = ros::Time::now();
	ros::Time last_request2 = ros::Time::now();

	int i = 0;
	int a = 0;

	if (!current_state.armed )
		if (arming_client.call(arm_cmd) && arm_cmd.response.success)
		{
			ROS_INFO("Vehicle armed");
		}


	while(current_state.mode != "OFFBOARD"){

		if (current_state.mode != "OFFBOARD"
				&& (ros::Time::now() - last_request > ros::Duration(5.0)))
		{
			if (set_mode_client.call(offb_set_mode) && offb_set_mode.response.mode_sent)
			{
				ROS_INFO("Offboard enabled");
			}
			last_request = ros::Time::now();
		}
		else
		{
			if (!current_state.armed && (ros::Time::now() - last_request > ros::Duration(5.0)))
			{
				ROS_INFO("Trying to arm");
				if (arming_client.call(arm_cmd) && arm_cmd.response.success)
				{
					ROS_INFO("Vehicle armed");
				}
				last_request = ros::Time::now();
			}
		}
		//	ROS_INFO("STILL TRYING ");
		if (ros::Time::now() - last_request2 > ros::Duration(3.0))
		{
			std::cout << "Mavros message"  << std::endl;
			std::cout << current_state.mode << std::endl;
			std::cout << current_state.armed << std::endl;

			std::cout << "String and bool message"  << std::endl;
			std::cout << lastStateReceived << std::endl;
			std::cout << isArm << std::endl;

			std::cout <<"all state   " <<current_state<< std::endl;
			last_request2 = ros::Time::now();
		}
		local_pos_pub.publish(pose);
		ros::spinOnce();
		rate.sleep();
	}
	while (ros::ok())
	{
		ROS_INFO("In main loop");
		/*			if (current_state.mode != "OFFBOARD"
					&& (ros::Time::now() - last_request > ros::Duration(5.0)))
			{
				if (set_mode_client.call(offb_set_mode) && offb_set_mode.response.mode_sent)
				{
					ROS_INFO("Offboard enabled");
				}
				last_request = ros::Time::now();
			}
			else
			{
				if (!current_state.armed && (ros::Time::now() - last_request > ros::Duration(5.0)))
				{
					ROS_INFO("Trying to arm");
					if (arming_client.call(arm_cmd) && arm_cmd.response.success)
					{
						ROS_INFO("Vehicle armed");
					}
					last_request = ros::Time::now();
				}
			}*/
		/*Change pose every 2 sec*/
		if ((ros::Time::now() - last_request2 > ros::Duration(2))) {
			if (a == 0){
				pose.pose.position.z = 2;
				a= 1;
				last_request2 = ros::Time::now();
				ROS_INFO("Pose z = 2");
			}
			else
			{
				pose.pose.position.z = 0;
				a= 0;
				last_request2 = ros::Time::now();
				ROS_INFO("Pose z = 0");
			}
		}

		//
		//		i++;
		//		if (i%100 )
		//			thrust.thrust += 10;
		//		if (i > 990)
		//		{
		//			i = 0;
		//			thrust.thrust = 0;
		//		}
		//
		//		std::cout << "ok publish thrust = "<<thrust.thrust << std::endl;
		//		ROS_INFO("  Pose x,y,z = %f , %f, %f",
		//				pose.pose.position.x,  pose.pose.position.y, pose.pose.position.z );
		local_pos_pub.publish(pose);
		//		set_thrust.publish(thrust);

		//setpoint_velocity.publish(velocity);

		ros::spinOnce();
		rate.sleep();
	}

	return 0;
}







































